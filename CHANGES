1.7.6 - 29 / 10 / 20

Misc:
- add option to forge operations only remotely
- now uses vuejs runtime directly

1.7.5 - 16 / 10 / 20

Bugfixes:
- fix some issues with api's onstatechanged feature when used with promises

1.7.4 - 08 / 10 / 20

Bugfixes:
- fix some issues with api's onstatechanged feature
- fix some issues with api's callback manager

1.7.3 - 29 / 09 / 20

Bugfixes:
- fix some issues with notif on error
- fix some issues with custom network
- fix some issues with love's timestamp

1.7.2 - 22 / 09 / 20

Bugfixes:
- fix some issues with notif not closing
- fix some issues on firefox version

1.7.1 - 18 / 09 / 20

New / Improved features:
- allow choice of unit for operation ("mudun" or "dun")

Bugfixes:
- fix postpone feature cleanup after commit
- fix clear of local storage (pending op, pending notif, etc) when updating storage
- fix api's callbacks not carrying error message
- small ui bugs

1.7 - 3 / 09 / 20

New / Improved features:
- full version of metal using vuejs
- add theming
- api now allows to pick the network
- exposes some node RPCs to the api
- add "expert mode" feature which allows batching / delaying of operations
- add next_time_block function in api

1.6 - 2 / 06 / 20

New / Improved features:
- first version of metal using vuejs
- improve api documentation

Misc
- change build system


1.5.1 - 27 / 05 / 20

Improved feature:
- default gas limit is now given by the node

Bugfixes:
- fix notarize hashing function
- fix minor bugs with custom networks

1.5 - 30 / 04 / 20

New / Improved features:
- support for batch operations feature
- support for collect call feature
- update Love to the latest version
- documentation improvements (examples and tutorials)
- allow emptying of account
- add promise support in API

Misc:
- update notarization contract on testnet
- small improvement in storage management
- continue work on store compliance

1.4 - 26 / 02 / 20

New / Improved Features:
  - support for custom network (allows usage of local nodes, sandbox network, etc)
  - make dunscan API endpoint optional  (this make working with sandbox networks easier)
  - notarization feature
  - LOVE smart-contract langage is now supported
  - new manager operation options

Misc:
  - update some encoding to the last revision of dune network
  - lot of work to be add-on/extension store compliant

beta4 - 05 / 02 / 20

New / Improved Features:
  - Babylon+ support (KT1 that becomes manager contract can send / delegate tokens)
  - allow unsetting of delegates
  - manage account operation (max_roll, whitelist, etc)
  - support of entrypoint in contract call

Misc:
  - first version for firefox
  - update manifest for store listing process

beta3 - 17 / 12 / 19

New / Improved Features :
  - Add full-delegation support to API (with ui)
  - allow the use of a custom node for activation
  - add a option to recover an account (with mnemonic or private key)

Improvements in UI :
  - improve display of notification in home page
  - add a presentation of metal on first setup
  - add some animation to smooth the load time and page changes
  - add error handling when the node rejects an operation

Bugfixes :
  - change the way we approve domain (and check if the domain is approved)
  - various issues with ledger (bridge, deny handler)
  - various issues with reveal
  - error margin in gas estimation (100 gas)
  - various issues when emptying the storage

Misc :
  - Build and dependency documentation
  - Api documentation

beta2 - 03 / 12 / 19

Improvements:
  - only send activation when account is not activated yet
  - better handling of reveal (now network dependent)
  - disable delegation on non-delegatable account

Bugfixes :
  - various issues with navbar
  - typos

beta1 - 29 / 11 / 19

Initial public release
