console.log("[metal-inpage] injected")

// Request's ID management
const MAX = 4294967295
let idCounter = Math.floor(Math.random() * MAX)

function getUniqueId () {
  idCounter = (idCounter + 1) % MAX
  return idCounter
}

// Callbacks management
var state_cb = undefined
let cbs = {}

function addCb(id, cb) {
  if (cb != undefined) {
    cbs[id] = cb ;
  }
}

function triggerCb(res) {
  if (cbs[res.id] != undefined) {
    if (typeof(res.result)=='object') cbs[res.id]({ ok : res.ok, ...res.result });
    else if (res.ok) cbs[res.id](res.result);
    else cbs[res.id]({ok: false, msg: res.result});
  }
}

window.addEventListener("message", function(event) {
  if (event.source != window) return
  try {
    var obj = event.data ;
    if (obj.src && (obj.src=="background" || obj.src=="popup")) {
      if (obj.state_change != undefined && state_cb != undefined) state_cb(obj.state_change);
      else if (obj.res != undefined) triggerCb(obj.res);
    }
  }
  catch(error) { console.log(error) }
}, false)

function post (method, cb, data) {
  var uid = getUniqueId().toString() ;
  var request = { id: uid, name: method, data: data } ;
  var p;
  if (cb != undefined) {
    addCb(uid, cb) ;
    window.postMessage({ src: "inpage", req: request }, "*")
  }
  else {
    p = new Promise(function(resolve, reject) {
      window.addEventListener("message", function(event) {
        if (event.source == window) {
          try {
            var obj = event.data;
            var res = obj.res;
            if (obj.src && (obj.src=="background" || obj.src=="popup")) {
              if (res != undefined && res.id == uid) {
                if (res.ok) resolve(res.result);
                else reject(res.result);
              }
            }
          }
          catch(error) { reject(error) } }
      }, false);
      window.postMessage({ src: "inpage", req: request }, "*")
    });
  }
  return p;
}

function relative_position(position) {
  var x = position != undefined && position.x != undefined ? position.x : 50;
  var y = position != undefined && position.y != undefined ? position.y : 100;
  return {x: window.screenX + x, y: window.screenY + y}
}

function make_op (kind, data) {
  if (kind == "transaction") {
    return {
      kind,
      info: {
        fee: data.fee,
        gasLimit: data.gas_limit,
        storageLimit: data.storage_limit,
      },
      transaction: {
        destination: data.dst,
        amount: data.amount,
        currency: data.currency,
        parameters: {
          entrypoint: data.entrypoint,
          value: data.parameter
        },
        collectCall: typeof(data.collect_call) == 'undefined' ? false : data.collect_call
      }
    };
  } else if (kind == "origination") {
    return {
      kind,
      info: {
        fee: data.fee,
        gasLimit: data.gas_limit,
        storageLimit: data.storage_limit,
      },
      origination: {
        balance: data.balance,
        currency: data.currency,
        script: {
          code: data.sc_code,
          codeHash: data.sc_code_hash,
          storage: data.sc_storage
        }
      }
    }
  } else if (kind == "delegation") {
    return {
      kind,
      info: {
        fee: data.fee,
        gasLimit: data.gas_limit,
        storageLimit: data.storage_limit,
      },
      delegation: {
        delegate: data.delegate
      }
    }
  } else {
    console.log("[inpage] type of operation not handled", kind)
  }
}


// metal state = { enabled ; unlocked ; approved ; selected_account }
// onstatechanged_listener : trigger when state is changed

metal = {

  /**
   * Callback using a boolean.
   *
   * @callback boolCallback
   * @param {bool} res
   */

  /**
   * Callback using the hash of the linked account.
   *
   * @callback hashCallback
   * @param {string} hash - hash of the linked account
   */

  /**
   * Generic object callback.
   *
   * @callback genCallback
   * @param {Object} res
   * @param {bool} res.ok - success of the request
   * @param {string} [ res.msg ] - error message or success message
   */

  /**
   * Callback using information on selected network.
   *
   * @callback networkCallback
   * @param {Object} res
   * @param {bool} res.ok - success of the request
   * @param {string} res.name - name of the network ("Mainnet", "Testnet" or "Custom")
   * @param {string} res.url - url of the node that metal uses to inject operations and query balance
   */

  /**
   * Callback using a delegation or transaction or batch operation result.
   *
   * @callback opCallback
   * @param {Object} res
   * @param {bool} res.ok - success of the operation
   * @param {string} res.msg - operation hash
   */

  /**
   * Callback using an origination result.
   *
   * @callback originationCallback
   * @param {Object} res
   * @param {bool} res.ok - success of the operation
   * @param {Object} res.msg - operation information
   * @param {string} res.msg.op - operation hash
   * @param {string} res.msg.contract - contract originated by the operation
   */

  /**
   * Position object
   *
   * @object positionObject
   * @param {number} [ res.x = 50 ] - horizontal position
   * @param {number} [ res.y = 100 ] - vertical position
   */

  /**
   * Get state of metal extension, enabled means the extension have been
   * setup (first account has been created)
   * @function isEnabled
   * @param {boolCallback} [ cb ] - callback to handle the result
   */
  isEnabled: function (cb) {
    return post("is_enabled", cb)
  },

  /**
   * Get state of metal extension, check if the extension have been unlocked
   * (user has enter is password)
   * @function isUnlocked
   * @param {boolCallback} [ cb ] - callback to handle the result
   */
  isUnlocked: function (cb) {
    return post("is_unlocked", cb)
  },

  /**
   * Get state of metal extension, approved means the domain have been approved
   * by the user (before sending sign requests a domain must be approve by the
   * user)
   * @function isApproved
   * @param {boolCallback} [ cb ] - callback to handle the result
   */
  isApproved: function (cb) {
    return post("is_approved", cb)
  },

  /**
   * Get the selected account hash
   * @function getAccount
   * @param {hashCallback} [ cb ] - callback to handle the result
   * @param {positionObject} [ position ] - relative position to the screen
   * @tutorial account_information
   */
  getAccount: function (cb, position=undefined) {
    return post("get_account", cb, {position: relative_position(position)})
  },


  /**
   * Get the selected network and its node address
   * @function getNetwork
   * @param {networkCallback} [ cb ] - callback to handle the result
   * @param {positionObject} [ position ] - relative position to the screen
   * @tutorial account_information
   */
  getNetwork: function (cb, position=undefined) {
    return post("get_network", cb, {position: relative_position(position)})
  },

  /**
   * Send dune tokens
   * @function send
   * @param {Object} obj
   * @param {string} obj.dst - destination account hash
   * @param {string} obj.amount - amount in mudun or in the unit given in currency
   * @param {string} [ obj.currency = mudun ] - currency of the amount
   * @param {string} [ obj.fee = auto ] - fee in mudun
   * @param {string} [ obj.entrypoint = default ] - entry point
   * @param {string} [ obj.parameter ] - json or michelson format parameter
   * @param {string} [ obj.gasLimit = auto ] - specify gas limit
   * @param {string} [ obj.storageLimit = auto ] - specify storage limit
   * @param {bool}   [ obj.collect_call = false ] - call a contract with collect call
   * @param {string} [ obj.network ] - specify the network required for the transaction
   * @param {string} [ obj.type ] - 'direct' or 'batch'
   * @param {positionObject} [ obj.position ] - relative position to the screen
   * @param {opCallback} [ obj.cb ] - callback with the hash of the injected operation as param
   * @example <caption>Example of simple transfer</caption>
   * metal.send({
   *   dst: 'dn1ZMhXSWscsCY5NS7LCGJEz2oYfShn73ASg',
   *   amount: '1000000',
   *   cb: (res => console.log(res.msg))
   * });
   * @example <caption>Example of contract call</caption>
   * metal.send({
   *   dst: 'KT1QbLD53PQqB67SB3GK3u8Qi6TbmwjVH2bd',
   *   amount: '0',
   *   entrypoint: 'default',
   *   parameter: '{"prim":"Pair","args":[{"bytes":"afb9356310150040303ed06ee1add1ff14f1d7181d1221ca3fbfed34f36a0725"},{"bytes":"544143202d2044414e49454c2e706466"}]}',
   *   cb: (res => console.log(res.msg))
   * });
   * @tutorial transaction_form
   */
  send: function({dst, amount, currency, fee, entrypoint, parameter, gas_limit, storage_limit, collect_call, network, type, position, cb}) {
    var op = make_op(
      "transaction",
      {dst, amount, currency, fee, entrypoint, parameter, gas_limit, storage_limit, collect_call});
    return post("send", cb, {op, network, type, position: relative_position(position)});
  },

  /**
   * Originate a new contract
   * @function originate
   * @param {Object} obj
   * @param {string} obj.balance - balance in mudun or in the unit given in currency
   * @param {string} [ obj.currency = mudun ] - currency of the balance
   * @param {string} [ obj.fee = auto ] - fee in mudun
   * @param {string} [ obj.gasLimit = auto ] - specify gas limit
   * @param {string} [ obj.storageLimit = auto ] - specify storage limit
   * @param {string} obj.sc_code_hash - json or michelson format contract code hash
   * @param {string} obj.sc_storage - json or michelson format contract storage
   * @param {string} obj.sc_code - json or michelson format contract code
   * @param {string} [ obj.network ] - specify the network required for the transaction
   * @param {string} [ obj.type ] - 'direct' or 'batch'
   * @param {positionObject} [ obj.position ] - relative position to the screen
   * @param {originationCallback} [ obj.cb ] - callback with the information of the injected operation as param
   * @example
   * metal.originate({
   *   balance: '0',
   *   sc_code:
   *     '[{"prim":"parameter","args":[{"prim":"pair","args":[{"prim":"bytes"},{"prim":"bytes"}]}],
   *      "annots":["%notarize"]},{"prim":"storage","args":[{"prim":"unit"}]},{"prim":"code","args":[[{"prim":"DUP"},
   *      {"prim":"DIP","args":[[{"prim":"CDR","annots":["@__slash_1"]}]]},{"prim":"CAR","annots":["@_document_hash__meta_data_slash_2"]},
   *      {"prim":"PUSH","args":[{"prim":"nat"},{"int":"16"}]},{"prim":"DIP","args":[[{"prim":"DUP"}]]},{"prim":"SWAP"},
   *      {"prim":"CAR","annots":["@document_hash"]},{"prim":"SIZE"},{"prim":"COMPARE"},{"prim":"LT"},
   *      {"prim":"IF","args":[[{"prim":"UNIT"},{"prim":"FAILWITH"}],[{"prim":"UNIT"}]]},{"prim":"DROP"},
   *      {"prim":"DROP","args":[{"int":"2"}]},{"prim":"UNIT"},{"prim":"NIL","args":[{"prim":"operation"}]},
   *      {"prim":"PAIR"}]]}]',
   *   sc_storage: '{"prim":"Unit"}',
   *   cb: (res => console.log(res.msg.op, res.msg.contract))
   * });

   */
  originate: function({balance, currency, fee, gas_limit, storage_limit,
                       sc_code_hash, sc_storage, sc_code, network, type, position, cb}) {
    var op = make_op(
      "origination",
      {balance, currency, fee, gas_limit, storage_limit, sc_code_hash, sc_storage, sc_code, type});
    return post("originate", cb, {op, network, type, position: relative_position(position)})
  },

  /**
   * Delegate
   * @function delegate
   * @param {Object} obj
   * @param {string} [ obj.delegate = unset ] - new delegate
   * @param {string} [ obj.fee = auto ] - fee in mudun
   * @param {string} [ obj.gasLimit = auto ] - specify gas limit
   * @param {string} [ obj.storageLimit = auto ] - specify storage limit
   * @param {string} [ obj.network ] - specify the network required for the transaction
   * @param {string} [ obj.type ] - 'direct' or 'batch'
   * @param {positionObject} [ obj.position ] - relative position to the screen
   * @param {opCallback} [ obj.cb ] - callback with the hash of the injected operation as param
   * @example
   * metal.delegate({
   *   delegate: 'dn1ZMhXSWscsCY5NS7LCGJEz2oYfShn73ASg',
   *   cb: (res => console.log(res.msg))
   * });
   */
  delegate: function({delegate, fee, gas_limit, storage_limit, network, type, position, cb}) {
    var op = make_op(
      "delegation",
      {delegate, fee, gas_limit, storage_limit});
    return post("delegate", cb, {op, network, type, position: relative_position(position)})
  },


  /**
   * Register a callback that will be trigger on metal state changes
   * (as of now only trigger on lock changes)
   * @function onStateChanged
   * @param {genCallback} cb
   * @example
   * metal.onStateChanged((r) =>
   *   console.log(r);
   *   metal.getAccount((hash) => console.log('account selected is now', hash));
   *   metal.getNetwork((res) => console.log('network selected is now', res.name, 'with address', res.url));
   * );
   * @tutorial account_information
   */
  onStateChanged: function (cb) {
    state_cb = cb
  },

  /**
   * Batched operations
   * @function batch
   * @param {Object} obj
   * @param {Object[]} obj.operations
   * @param {string} obj.operations[].kind - kind of operation
   * @param {Object} obj.operations[].operation - operation (send, originate or delegate) as further described
   * @param {string} [ obj.network ] - specify the network required for the transaction
   * @param {string} [ obj.type ] - 'direct' or 'batch'
   * @param {positionObject} [ obj.position ] - relative position to the screen
   * @param {opCallback} [ obj.cb ] - callback with the hash of the injected operation as param
   */
  batch: function({operations, network, type, position, cb}) {
    function make(op) { return make_op(op.kind, op.operation) };
    var ops = operations.map(make);
    return post("batch", cb, {ops, network, type, position: relative_position(position)})
  },

  /**
   * Node RPC
   * @function rpc
   * @param {Object} obj
   * @param {string} obj.rpc - rpc ('chains/main/blocks/head' for example)
   * @param {string} [ obj.network ] - optional network ('mainnet' for example)
   * @param {positionObject} [ obj.position ] - relative position to the screen
   * @param {genCallback} [ obj.cb ] - callback with the result of the RPC as param
   */
  rpc: function({rpc, network, position, cb}) {
    return post("get_rpc", cb, {rpc, network, position: relative_position(position)})
  },

  /**
   * Node head
   * @function head
   * @param {Object} [ obj ]
   * @param {string} [ obj.network ] - optional network ('mainnet' for example)
   * @param {positionObject} [ obj.position ] - relative position to the screen
   * @param {genCallback} [ obj.cb ] - callback with the head block as param
   */
  head: function(obj) {
    var network = obj ? obj.network : undefined;
    var position = obj ? obj.position : undefined;
    var cb = obj ? obj.cb : undefined;
    var data = {rpc : 'chains/main/blocks/head', network, position: relative_position(position)};
    return post("get_rpc", cb, data)
  },

  /**
   * Get the next time (in seconds) a block will be baked (assuming priority 0)
   * @function next_block_time
   * @param {Object} [ obj ]
   * @param {string} [ obj.network ] - optional network ('mainnet' for example)
   * @param {positionObject} [ obj.position ] - relative position to the screen
   * @param {genCallback} [ obj.cb ] - callback with time as param
   */
  next_block_time: function(obj) {
    var network = obj ? obj.network : undefined;
    var position = obj ? obj.position : undefined;
    if (obj == undefined || obj.cb == undefined) {
      var data = {rpc : 'chains/main/blocks/head/context/constants', network, position};
        return post("get_rpc", cb, data).then(
            cst => {
                var delay = cst.time_between_blocks[0] * 1000 ;
              var data = {rpc : 'chains/main/blocks/head/header/shell', network, position};
                return post("get_rpc", cb, data).then(
                    header => {
                        var last_block_time = new Date(header.timestamp).getTime();
                        var next_block_time = new Date().getTime() - last_block_time ;
                        var rem_time = delay - next_block_time ;
                        var rem_time = rem_time >= 0 ? rem_time / 1000 : 0 ;
                        return rem_time
                    })
            })
    } else
      {
          var cb =
              cst => {
                  var delay = cst.time_between_blocks[0] * 1000 ;
                var data = {rpc : 'chains/main/blocks/head/header', network, position};
                  var cb =
                      header => {
                          var last_block_time = new Date(header.timestamp).getTime();
                          var next_block_time = new Date().getTime() - last_block_time ;
                          var rem_time = delay - next_block_time ;
                          var rem_time = rem_time >= 0 ? rem_time / 1000 : 0 ;
                          obj.cb(rem_time)
                      };
                  post("get_rpc", cb, data);
              };
        var data = {rpc : 'chains/main/blocks/head/context/constants', network, position};
          post("get_rpc", cb, data);
      } ;
  },

  /**
   * Commit the background batch list
   * @function commit
   * @param {Object} [ obj ]
   * @param {string} [ obj.network ] - optional network ('mainnet' for example)
   * @param {positionObject} [ obj.position ] - relative position to the screen
   * @param {opCallback} [ obj.cb ] - callback with the hash of the injected operation as param
   */
  commit: function(obj) {
    var network = obj ? obj.network : undefined;
    var position = obj ? obj.position : undefined;
    var data = {network, position: relative_position(position)};
    var cb = obj ? obj.cb : undefined;
    return post("commit_batch", cb, data)
  },

}
