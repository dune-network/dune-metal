(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

module Misc = struct
  let spf fmt = Printf.sprintf fmt

  let unoptf def f = function
    | None -> def
    | Some x -> f x

  let unopt def = function
    | None -> def
    | Some x -> x

  let unopt_exn = function
    | None -> assert false
    | Some x -> x

  let convopt f = function
    | None -> None
    | Some x -> Some (f x)

  let filter_map f l =
    List.rev @@ List.fold_left (fun acc x ->
        match f x with None -> acc | Some x -> x :: acc) [] l

  let shuffle l =
    List.map (fun v -> Random.int 1_000_000, v) l
    |> List.sort (fun (i, _) (i', _) -> compare i i')
    |> List.map snd

  let rec sublist ?(start=0) length = function (* not tail rec *)
    | [] when start > 0 -> assert false
    | [] -> []
    | _ when length = 0 -> []
    | _ :: t when start > 0 -> sublist ~start:(start-1) length t
    | h :: t -> h :: sublist (length-1) t

  let unopt_list f l =
    List.rev @@ List.fold_left (fun acc elt -> match elt with
        | None -> acc
        | Some elt -> f elt :: acc) [] l
end

module Res = struct
  type metal_error =
    | Str_err of string
    | Gen_err of (int * string option)
    | Exn_err of exn list

  let error_content = function
    | Str_err s -> (0, Some s)
    | Gen_err (code, content) -> (code, content)
    | Exn_err e -> (1, Some (String.concat "\n" @@ List.map Printexc.to_string e))

  let error_content0 ?(none="No content") = function
    | Str_err s -> (400, s)
    | Gen_err (code, content) -> (code, Misc.unopt none content)
    | Exn_err e -> (500, (String.concat "\n" @@ List.map Printexc.to_string e))

  let error e = Error e
  let ok x = Ok x
  let gen_err (code, content) = Gen_err (code, content)
  let conv ~error ~ok = function
    | Error e -> Error (error e)
    | Ok x -> Ok (ok x)


  let (>>?) v f =
    match v with
    | Error _ as err -> err
    | Ok v -> f v

  let (>|?) v f = v >>? fun v -> Ok (f v)

  let map_res f l =
    match List.fold_left (fun acc x -> match acc with
        | Error e -> Error e
        | Ok acc -> match f x with
          | Error e -> Error e
          | Ok x -> Ok (x :: acc)) (Ok []) l with
    | Error e -> Error e
    | Ok l -> Ok (List.rev l)

  let mapi_res f l =
    match List.fold_left (fun (i, acc) x -> match acc with
        | Error e -> i+1, Error e
        | Ok acc -> match f i x with
          | Error e -> i+1, Error e
          | Ok x -> i+1, Ok (x :: acc)) (0, Ok []) l with
    | _, Error e -> Error e
    | _, Ok l -> Ok (List.rev l)

  let map2_res f l1 l2 =
    match List.fold_left2 (fun acc x1 x2 -> match acc with
        | Error e -> Error e
        | Ok acc -> match f x1 x2 with
          | Error e -> Error e
          | Ok x -> Ok (x :: acc)) (Ok []) l1 l2 with
    | Error e -> Error e
    | Ok l -> Ok (List.rev l)

  let map2i_res f l1 l2 =
    match List.fold_left2 (fun (i, acc) x1 x2 -> match acc with
        | Error e -> i+1, Error e
        | Ok acc -> match f i x1 x2 with
          | Error e -> i+1, Error e
          | Ok x -> i+1, Ok (x :: acc)) (0, Ok []) l1 l2 with
    | _, Error e -> Error e
    | _, Ok l -> Ok (List.rev l)

  let print_error e =
    let code, content = error_content0 e in
    Printf.eprintf "Error %d: %s\n%!" code content

  let print_result f = function
    | Ok x -> f x
    | Error e -> print_error e

  let print_uresult = function
    | Ok () -> ()
    | Error e -> print_error e
end

include Misc
include Res
