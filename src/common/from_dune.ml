module Dune = Dune_types
module Metal = Metal_types
open Dune

let dune_errors = function
  | None -> None
  | Some meta -> match meta.manager_meta_operation_result with
    | None -> None
    | Some meta ->
      Some (List.map (fun {node_err_kind; node_err_id; node_err_info} ->
          {Metal.err_kind = node_err_kind; err_id = node_err_id; err_info = node_err_info}) meta.meta_op_errors)

let dune_failed = function
  | None -> false
  | Some meta -> match meta.manager_meta_operation_result with
    | None -> false
    | Some meta -> match meta.meta_op_status with
      | None -> false
      | Some status -> status <> "applied"

let dune_params e v = match e, v with
  | None, None -> None
  | e, v -> Some (e, v)

let dune_script = function
  | None -> None
  | Some {sc_code; sc_storage; sc_code_hash} -> Some (sc_code, sc_storage, sc_code_hash)

let get_burn = function
  | None -> 0L
  | Some meta -> match meta.manager_meta_operation_result with
    | None -> 0L
    | Some meta ->
      match meta.meta_op_balance_updates with
      | None -> 0L
      | Some l_bu ->
        Int64.neg @@
        List.fold_left (fun acc bu -> match bu with
            | Contract (_, change) -> Int64.add acc change
            | _ -> acc) 0L l_bu

let get_internal_burn = function
  | None -> 0L
  | Some meta ->
    List.fold_left (fun acc -> function
        | NTransaction transaction ->
          Int64.add acc (get_burn transaction.node_tr_metadata)
        | NOrigination origination ->
          Int64.add acc (get_burn origination.node_or_metadata)
        | _ -> acc)
      0L meta.manager_meta_internal_operation_results

let dune_burn meta =
  let burn = Int64.add (get_burn meta) (get_internal_burn meta) in
  if burn = 0L then None else Some burn

let dune_op ?op_bytes = function
  | NTransaction tr -> Some {
      Metal.mo_info = {
        Metal.mi_source = tr.node_tr_src;
        mi_fee = Some tr.node_tr_fee;
        mi_counter = Some (Z.to_int tr.node_tr_counter);
        mi_gas_limit = Some tr.node_tr_gas_limit;
        mi_storage_limit = Some tr.node_tr_storage_limit;
        mi_burn = dune_burn tr.node_tr_metadata;
        mi_errors = dune_errors tr.node_tr_metadata ;
        mi_failed = dune_failed tr.node_tr_metadata;
        mi_internal = tr.node_tr_counter = Z.minus_one };
      mo_det = Metal.TraDetails {
        Metal.trd_dst = tr.node_tr_dst;
        trd_amount = tr.node_tr_amount;
        trd_parameters = dune_params tr.node_tr_entrypoint tr.node_tr_parameters;
        trd_collect_call = match tr.node_tr_collect_fee_gas with
          | None -> false
          | Some _fee -> true
      }
    }
  | NOrigination ori -> Some {
      Metal.mo_info = {
        Metal.mi_source = ori.node_or_src;
        mi_fee = Some ori.node_or_fee;
        mi_counter = Some (Z.to_int ori.node_or_counter);
        mi_gas_limit = Some ori.node_or_gas_limit;
        mi_storage_limit = Some ori.node_or_storage_limit;
        mi_burn = dune_burn ori.node_or_metadata;
        mi_errors = dune_errors ori.node_or_metadata ;
        mi_failed = dune_failed ori.node_or_metadata;
        mi_internal = ori.node_or_counter = Z.minus_one };
      mo_det = Metal.OriDetails {
          Metal.ord_balance = ori.node_or_balance;
          ord_kt1 = (match op_bytes with None -> None | Some b -> Some (MCrypto.op_to_KT1 b));
          ord_delegate = None;
          ord_script = dune_script ori.node_or_script }
    }
  | NReveal rvl -> Some {
      Metal.mo_info = {
        Metal.mi_source = rvl.node_rvl_src;
        mi_fee = Some rvl.node_rvl_fee;
        mi_counter = Some (Z.to_int rvl.node_rvl_counter);
        mi_gas_limit = Some rvl.node_rvl_gas_limit;
        mi_storage_limit = Some rvl.node_rvl_storage_limit;
        mi_burn = None;
        mi_errors = dune_errors rvl.node_rvl_metadata ;
        mi_failed = dune_failed rvl.node_rvl_metadata;
        mi_internal = rvl.node_rvl_counter = Z.minus_one };
      mo_det = Metal.RvlDetails rvl.node_rvl_pubkey
    }
  | NDelegation del -> Some {
      Metal.mo_info = {
            Metal.mi_source = del.node_del_src;
            mi_fee = Some del.node_del_fee;
            mi_counter = Some (Z.to_int del.node_del_counter);
            mi_gas_limit = Some del.node_del_gas_limit;
            mi_storage_limit = Some del.node_del_storage_limit;
            mi_burn = None;
            mi_errors = dune_errors del.node_del_metadata ;
            mi_failed = dune_failed del.node_del_metadata;
            mi_internal = del.node_del_counter = Z.minus_one};
      mo_det = Metal.DelDetails del.node_del_delegate
    }
  | NManage_account mac ->
    let target = MMisc.convopt fst mac.node_mac_target in
    Some {
      Metal.mo_info = {
            Metal.mi_source = mac.node_mac_src;
            mi_fee = Some mac.node_mac_fee;
            mi_counter = Some (Z.to_int mac.node_mac_counter);
            mi_gas_limit = Some mac.node_mac_gas_limit;
            mi_storage_limit = Some mac.node_mac_storage_limit;
            mi_burn = None;
            mi_errors = dune_errors mac.node_mac_metadata ;
            mi_failed = dune_failed mac.node_mac_metadata ;
            mi_internal = mac.node_mac_counter = Z.minus_one};
      mo_det = Metal.ManDetails (
          target, match mac.node_mac_options with
          | MaoDecoded mao -> Metal.MadDecoded {
              Metal.mao_maxrolls = mao.node_mao_maxrolls;
              mao_admin = mao.node_mao_admin;
              mao_white_list = mao.node_mao_white_list;
              mao_delegation = mao.node_mao_delegation;
              mao_recovery = mao.node_mao_recovery;
              mao_actions = mao.node_mao_actions;
            }
          | MaoJSON s -> Metal.MadJSON s)
    }
  | _ -> None

let dune_ops ?op_bytes bo_hash filter l = {
  Metal.bo_hash;
  bo_block = None;
  bo_branch = None;
  bo_op =
    List.rev @@ List.fold_left (fun acc x ->
        match dune_op ?op_bytes x with
        | None -> acc
        | Some x -> match filter x with
          | None -> acc
          | Some x -> x :: acc) [] l }
