(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Metal_types

let to_fo_state {ho_acc; ho_net; _} = {fo_acc = ho_acc; fo_net = ho_net}
let to_ho_state {fo_acc; fo_net} ho_accs = {ho_acc = fo_acc; ho_net = fo_net; ho_accs}
let update_ho_state state ho_acc =
  let ho_accs = List.map (fun a -> if a.pkh = ho_acc.pkh then ho_acc else a)
      state.ho_accs in
  {state with ho_acc; ho_accs}

let network_to_string = function
  | Mainnet -> "Mainnet"
  | Testnet -> "Testnet"
  | Devnet  -> "Devnet"
  | Custom s -> s

let network_of_string = function
  | "Mainnet" | "mainnet" -> Mainnet
  | "Testnet" | "testnet" -> Testnet
  | "Devnet" | "devnet" -> Devnet
  | s -> Custom s

let dunscan_path ?(suffix="") = function
  | Testnet -> Printf.sprintf "https://testnet.%s/%s" Menv.external_host suffix
  | Devnet  -> Printf.sprintf "https://devnet.%s/%s" Menv.external_host suffix
  | _ -> Printf.sprintf "https://%s/%s" Menv.external_host suffix

let network_options = [Mainnet; Testnet; Devnet]

let state_to_string = function
  | Disabled -> "disabled"
  | Locked -> "locked"
  | Unlocked  -> "unlocked"

let state_of_string = function
  | Disabled -> "disabled"
  | Locked -> "locked"
  | Unlocked  -> "unlocked"

let failed_opt ?(failed=false) op =
  if not failed && op.mo_info.mi_failed then None else Some op

let transaction_opt ?failed op =
  match failed_opt ?failed op with
  | Some ({mo_det = (TraDetails mo_det); _} as op) -> Some { op with mo_det }
  | _ -> None

let origination_opt ?failed op =
  match failed_opt ?failed op with
  | Some ({mo_det = (OriDetails mo_det); _} as op) -> Some { op with mo_det }
  | _ -> None

let delegation_opt ?failed op =
  match failed_opt ?failed op with
  | Some ({mo_det = (DelDetails mo_det); _} as op) -> Some { op with mo_det }
  | _ -> None

let reveal_opt ?failed op =
  match failed_opt ?failed op with
  | Some ({mo_det = (RvlDetails mo_det); _} as op) -> Some { op with mo_det }
  | _ -> None

let manage_account_opt ?failed op =
  match failed_opt ?failed op with
  | Some ({mo_det = (ManDetails mo_det); _} as op) -> Some { op with mo_det }
  | _ -> None

let filter_operations f mos = MMisc.filter_map f mos

let filter_block_operations f bos =
  MMisc.filter_map (fun bo -> match f bo.bo_op with
      | None -> None
      | Some bo_op -> Some {bo with bo_op}) bos

let flatten_block_operations f bos =
  List.flatten @@ List.map (fun bo ->
      List.map (fun bo_op -> {bo with bo_op}) @@
      MMisc.filter_map f bo.bo_op) bos

let notif_id_of_not = function
  | OpNot n -> n.not_id
  | BatchNot n -> n.not_id
  | ApprovNot n -> n.not_approv_id

let notif_wid_of_not = function
  | OpNot n -> n.not_wid
  | BatchNot n -> n.not_wid
  | ApprovNot n -> n.not_approv_wid

let notif_tsp_of_not = function
  | OpNot n -> n.not_tsp
  | BatchNot n -> n.not_tsp
  | ApprovNot n -> n.not_approv_tsp

let parse_script = function
  | None -> None
  | Some s -> match Dune.json_of_string s with
    | Error _ -> None
    | Ok p -> Some p

let make_notif_op ?network ~id ~wid ~tsp ~origin not_op =
  OpNot {
    not_origin = origin; not_tsp = tsp; not_id = id; not_wid = wid ; not_op;
    not_network = network; not_type = None }

let make_notif_batch ?network ~id ~wid ~tsp ~origin not_op =
  BatchNot {
    not_origin = origin; not_tsp = tsp; not_id = id; not_wid = wid ; not_op;
    not_network = network; not_type = None }

let make_notif_approv ~id ~wid ~tsp ~icon ~name ~url =
  ApprovNot {
    not_approv_id = id ;
    not_approv_wid = wid ;
    not_approv_icon = icon ;
    not_approv_name = name ;
    not_approv_url = url ;
    not_approv_tsp = tsp ;
  }

let notarization_contract = function
  | Mainnet -> Some "KT1QbLD53PQqB67SB3GK3u8Qi6TbmwjVH2bd"
  | Testnet -> Some "KT1LB4x69cmneBCRTN5c3WUQgnTpwqZQVyUh"
  | _ -> None

let opt_network_assoc network a = List.assoc_opt network a

let get_network_assoc def network a = match List.assoc_opt network a with
  | None -> def
  | Some x -> x

let update_network_assoc network a x =
  (network, x) :: List.remove_assoc network a

let split_mem find l =
  let l1, l2 = List.fold_left (fun (acc1, acc2) x ->
      match find x with
      | None -> x :: acc1, acc2
      | Some y -> acc1, y :: acc2) ([], []) l in
  List.rev l1, List.rev l2

let dummy_account ?(vault=Local (Bigstring.empty, Bigstring.empty)) ?manager pkh = {
  pkh; manager = (match manager with None -> pkh | Some m -> m);
  vault; name = pkh; revealed = []; pending = []; history = []; manager_kt = None;
  admin = []; acc_index = -1 }

let dummy_fo_state ?vault ?manager ~network pkh =
  {fo_acc = dummy_account ?vault ?manager pkh; fo_net = network}

let is_op_kind kind det = match kind, det with
  | "Transaction", TraDetails _ -> true
  | "Origination", OriDetails _ -> true
  | "Delegation", DelDetails _ -> true
  | "Notarization", TraDetails _ -> true
  | "Manage_account", ManDetails _ -> true
  | _ -> false
