open MMisc

module Types = Dune_types
module Encoding = Dune_encoding
module Utils = Encoding.Utils
module Micheline = Micheline
module Love = Love

open Types

(* printers *)

let print_love_expr = function
  | LoveValue lv -> Format.asprintf "%a" Love_printer.Value.print lv
  | LoveType _lt -> ""

let print_love_code = function
  | LoveTopContract ltc ->
    Format.asprintf "%a" Love_printer.Ast.print_structure ltc.Love_ast_types.AST.code
  | LoveLiveContract llc ->
    Format.asprintf "%a"
      Love_printer.Value.print_livestructure llc.Love_value.LiveContract.root_struct
  | LoveFeeCode _lfc -> ""

let print_dune_expr = function LoveExpr le -> print_love_expr le
let print_dune_code = function LoveCode le -> print_love_code le

let print_script ?contract = function
  | Mich sc -> Micheline.Printer.to_string ?contract sc
  | DuneExpr de -> print_dune_expr de
  | DuneCode dc -> print_dune_code dc

let get_header s =
  if String.length s >= 1 && s.[0] = '#' then
    let rec iter s pos len =
      if pos = len then pos else
        match s.[pos] with
        | '\n' | '\r' | ':' | ' ' | '\t' -> pos
        | _ -> iter s (pos+1) len
    in
    let i = iter s 1 (String.length s) in
    if i - 1 <= 0 then None, 0
    else Some (String.sub s 1 (i - 1)), i + 1
  else None, 0

(* parsers *)

let parse_micheline s =
  let tokens, errors = Micheline.Parser.tokenize s in
  match errors with
  | _ :: _ -> Error (Exn_err errors)
  | [] ->
    let node, errors = Micheline.Parser.parse_expression ~check:false tokens in
    match errors with
    | _ :: _ -> Error (Exn_err errors)
    | [] -> Ok (Mich (Micheline.Types.strip_locations node))

let parse_json s =
  try Ok (Encoding.Script.decode s)
  with exn ->
    Error (Str_err (
        Printf.sprintf "Cannot parse json %S: %s" s (Printexc.to_string exn)))

let parse_love ?(contract=false) s =
  let lb = Lexing.from_string s in
  try (
    if contract then
      let code = Love_parser.top_contract Love_lexer.token lb in
      Ok (DuneCode (LoveCode (LoveTopContract code)))
    else
      match Love_parser.top_value Love_lexer.token lb with
      | Love_ast_types.AST.VALUE v -> Ok (DuneExpr (LoveExpr (LoveValue (Love_value.value_of_value v))))
      | Love_ast_types.AST.TYPE t -> Ok (DuneExpr (LoveExpr (LoveType t))))
  with exn ->
    Error (Str_err (Printf.sprintf "Cannot parse love %S: %s" s (Printexc.to_string exn)))

let parse_script ?contract s =
  let h, i = get_header s in
  let s = String.trim @@ String.sub s i ((String.length s) - i) in
  match h with
  | Some "mic" -> parse_micheline s
  | Some "love" -> parse_love ?contract s
  | Some "json" -> parse_json s
  | Some s -> Error (Str_err ("Cannot parse header " ^ s))
  | None -> parse_json s

(* string to json *)
let json_of_string ?contract s =
  let h, i = get_header s in
  let s = String.trim @@ String.sub s i ((String.length s) - i) in
  match h with
  | Some "mic" -> parse_micheline s >|? Encoding.Script.encode
  | Some "love" -> parse_love ?contract s >|? Encoding.Script.encode
  | Some "json" -> Ok s
  | Some s ->   Error (Str_err ("Cannot parse header " ^ s))
  | None -> Ok s
