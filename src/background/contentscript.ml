(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min
open Chrome
open Metal_message_types

let inject_api () =
  log_str "[metal-content-script] injecting api" ;
  let url = Runtime.getURL "inpage.js" in
  let container = Dom_html.window##.document##.documentElement in
  Opt.case
    (Dom.CoerceTo.element container)
    (fun () -> failwith "error while injecting : coerce")
    (fun container ->
       let script = Ezjs_dom.El.script [] in
       script##.async := _false ;
       script##.src := string url ;
       Dom.insertBefore container script null ;
       Dom.removeChild container script
    )

let get_site_name () =
  let doc = Dom_html.window##.document in
  let meta_site_name =
    match Opt.to_option @@
      doc##querySelector(string "head > meta[property=\"og:site_name\"]") with
    | Some meta ->
      begin match Opt.to_option @@ Dom_html.CoerceTo.meta meta with
        | Some meta -> Some meta##.content
        | None -> None
      end
    | None -> None in
  match meta_site_name with
  | Some sn -> sn
  | None ->
    begin
      match Opt.to_option @@
        doc##querySelector (string "head > meta[name=\"title\"]") with
      | Some meta ->
        begin match Opt.to_option @@ Dom_html.CoerceTo.meta meta with
          | Some meta -> meta##.content
          | None -> doc##.title
        end
      | None -> doc##.title
    end

let get_site_icon () =
  let doc = Dom_html.window##.document in
  let shortcut_icon =
    match Opt.to_option @@
      doc##querySelector (string "head > link[rel=\"shortcut icon\"]") with
    | Some icon ->
      begin match Opt.to_option @@ Dom_html.CoerceTo.link icon with
        | Some icon -> Some icon##.href
        | None -> None
      end
    | None -> None
  in
  match shortcut_icon with
  | Some icon -> Some icon
  | None ->
    begin match Opt.to_option @@
        doc##querySelector(string "head > link[rel=\"icon\"]") with
    | Some icon ->
      begin match Opt.to_option @@ Dom_html.CoerceTo.link icon with
        | Some icon -> Some icon##.href
        | None -> None
      end
    | None -> None
    end

let get_site_metadata () : site_metadata t =
  let name = get_site_name () in
  let icon = get_site_icon () in
  object%js
    val name = name
    val icon = Optdef.option icon
    val url = Dom_html.window##.location##.hostname
  end

let enrich_msg (msg : _ message t) =
  msg##.req##.metadata := def @@ get_site_metadata () ;
  msg

let () =
  (* Connect to the background and listens for message *)
  let info = Utils.Runtime.mk_connection_info "contentscript" in
  let port = Runtime.connect ~info () in
  Utils.Browser.addListener1
    (port##.onMessage)
    (fun msg ->
       (Unsafe.coerce Dom_html.window)##postMessage(msg)) ;
  (* Listens for message from inpage *)
  ignore @@
  Js_of_ocaml.Dom_events.listen
    Dom_html.window
    (Dom_html.Event.make "message")
    (fun _target ev ->
       Opt.case
         (ev##.srcElement)
         (fun () -> failwith "[Content-script] recveive a message with no source")
         (fun src ->
            if src = (Unsafe.coerce Dom_html.window) then
              try
                let (msg : _ message t) = ev##.data in
                let src = to_string msg##.src in
                if src = "inpage" then port##postMessage (enrich_msg msg)
                else ()
              with _ -> ()
            else ());
       true) ;
  inject_api () ;
