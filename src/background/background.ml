(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min
open Chrome
open MLwt

let print_conn_recv (port : Utils.Runtime.port t) =
  Optdef.case
    (port##.sender)
    (fun () -> ())
    (fun sender ->
       Optdef.case
         (sender##.url)
         (fun _ -> log_str "[Background] connection recv")
         (fun url ->
            log_str @@ "[Background] " ^ (to_string url) ^ " connected"))

let load_custom_networks () =
  let json_url = Runtime.getURL "json/custom_networks.json" in
  let service = EzAPI.service ~output:Metal_encoding.custom_networks_encoding
      EzAPI.Path.(root // json_url) in
  async (fun () ->
      Mrequest.Node.get0 ~msg:"custom_networks" (EzAPI.TYPES.BASE "") service
      >>|? Storage_writer.update_custom_networks >|= fun r ->
      print_uresult r)

let () =
  Ledger_bridge.make_iframe ();
  Browser_action.set_badge_bg ~color:"#c62828" ();
  Runtime.onConnect
    (fun port ->
       let port_name = to_string port##.name in
       if port_name = "ledger" then (
         Utils.Browser.addListener1
           (port##.onMessage)
           (fun msg ->
              if msg##.action = string "getAddress" || msg##.action = string "sign" then
                Ledger_bridge.send_message port msg))
       else (
         print_conn_recv port ;
         Handler.add_port port ;
         Utils.Browser.addListener1
           (port##.onMessage)
           (fun msg -> Handler.handle port msg) ;
         Utils.Browser.addListener1
           (port##.onDisconnect)
           (fun p ->
              log_str @@ "Disconnect " ^ (to_string p##.name) ;
              Handler.remove_port port))) ;

  Runtime.onInstalled (fun (event : Runtime.onInstalledEvent t) ->
      if event##.reason = string "install" then
        let tab = Tabs.make_create ~url:"index.html?page=presentation" () in
        Tabs.create tab);

  Storage_updates.update_storage ();
  load_custom_networks ();
  (* WARNING: would be useful to uncomment for the proper release *)
  (* Runtime.onUpdateAvailabale (fun _ -> Storage_types.update_storage ()) *)
