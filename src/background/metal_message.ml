(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min
open Metal_message_types

let mk_request ?data ~name id : 'a request t =
  object%js
    val id = id
    val name = string name
    val data = Optdef.option data
    val mutable metadata = undefined
  end

let mk_message ?data ~name ~src id : 'a message t =
  let req = mk_request ?data ~name @@ string id in
  object%js
    val src = string src
    val req = req
  end

let mk_metadata_req src rid =
  mk_message ~name:"get_metadata" ~src rid

let mk_op_req src rid =
  mk_message ~name:"get_opdata" ~src rid

let mk_state_changed_message ~src ?data () =
  let req = mk_request ?data ~name:"state_changed" @@ string "-1" in
  object%js
    val src = string src
    val req = req
  end

let mk_batch_req src rid =
  mk_message ~name:"get_batchdata" ~src rid

let mk_answer ?(ok=true) ~src id (res : 'a) : 'a answer t =
  let res = object%js
    val id = id
    val ok = bool ok
    val result = res
  end in
  object%js
    val src = string src
    val res = res
  end

let mk_err_answer ~src id errs =
  mk_answer ~ok:false ~src id @@ object%js
    val msg = array (Array.of_list (List.map string errs))
  end

let mk_state_changed_answer ?data src : _ state_change t =
  let data = match data with Some d -> d | None -> Unsafe.coerce (string "metal state changed") in
  object%js
    val src = string src
    val state_change_ = data
  end

let get_position p = to_optdef (fun p -> p##.x, p##.y) p
