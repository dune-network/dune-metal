(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Chrome
open Ezjs_min

(* Changes need to be made in static/scss/main.scss aswell *)
let notif_height = 600
let notif_width = 357

let port_cbs : (Utils.Runtime.port t, 'a list) Hashtbl.t = Hashtbl.create 100

let show_popup ?(position=(50,0)) port cb url =
  let should_open =
    try
      let cbs = Hashtbl.find port_cbs port in
      Hashtbl.replace port_cbs port (cb :: cbs) ;
      false
    with Not_found ->
      Hashtbl.add port_cbs port [ cb ] ;
      true in
  if should_open then
    let data =
      Windows.make_createData
        ~url_l:[ url ]
        ~typ:"popup"
        ~height:notif_height
        ~width:notif_width
        ~left:(fst position)
        ~top:(snd position)
        () in
    log_str "data"  ;
    Windows.create
      ~info:data
      ~callback:(fun popup ->
          js_log popup ;
          let id =
            Optdef.get
              (popup##.id)
              (fun () -> failwith "[notification] no id for fresh popup") in
          Windows.onRemoved
            (fun winid ->
               if winid = id then
                 try
                   let cbs = Hashtbl.find port_cbs port in
                   List.iter (fun cb -> cb ()) @@ List.rev cbs ;
                   Hashtbl.remove port_cbs port
                 with Not_found -> ()
               else ()))
      ()

let show_popup_action ?id ?position port cb action =
  log_str ("[notification] show_popup_" ^ action) ;
  let id_req = match id with None -> "" | Some id -> "&req_id=" ^ id in
  let url = "notification.html?type=" ^ action ^ id_req in
  show_popup ?position port cb url
