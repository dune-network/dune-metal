(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min
open Metal_message_types
open Metal_types
open Storage_utils
open Chrome
open MLwt

let pending_approved : (string * site_metadata t) list ref = ref []
let pending_op : (string * op t) list ref = ref []
let pending_batch : (string * batch t) list ref = ref []
let pending_config : (string * op_config t) list ref = ref []

let port_table : (string, Utils.Runtime.port t) Hashtbl.t = Hashtbl.create 100

let store_batch : manager_op t list ref = ref []

let send ?ok (port : Utils.Runtime.port t) (req : _ request t) res =
  let obj = Metal_message.mk_answer ?ok ~src:"background" req##.id res in
  port##postMessage (obj)

let send_error (port : Utils.Runtime.port t) (req : _ request t) errs =
  let obj = Metal_message.mk_err_answer ~src:"background" req##.id errs in
  port##postMessage (obj)

let state_changed_notif_ports = ref []

let add_port (port : Utils.Runtime.port t) =
  Optdef.case port##.sender
    (fun () -> ())
    (fun sender -> Optdef.case sender##.url
        (fun () -> ())
        (fun url ->
           state_changed_notif_ports :=
             (to_string url, port) :: !state_changed_notif_ports))

let remove_port (port : Utils.Runtime.port t) =
  Optdef.case port##.sender
    (fun () -> ())
    (fun sender -> Optdef.case sender##.url
        (fun () -> ())
        (fun url ->
           state_changed_notif_ports :=
             List.remove_assoc (to_string url) !state_changed_notif_ports))

let state_changed ?data () =
  log_str "state_changed" ;
  js_log !state_changed_notif_ports ;
  let notif = Metal_message.mk_state_changed_answer ?data "popup" in
  List.iter (fun (_url, (port : Utils.Runtime.port t)) -> port##postMessage notif)
    !state_changed_notif_ports

(* TODO : background_utils *)
let update_badge () =
  Storage_reader.get_account @@ fun acc _ ->
  Storage_writer.get_notifs @@ fun ns ->
  match acc with
  | None -> ()
  | Some account ->
    let n_notif =
      List.length @@
      try
        (List.find (fun {notif_acc ; _} ->
             notif_acc = account.pkh) ns).notifs
      with Not_found -> [] in
    let n_notif = if !store_batch <> [] then n_notif + 1 else n_notif in
    Chrome.Browser_action.set_badge
      ~text:(if n_notif = 0 then "" else string_of_int n_notif) ()

let get_last_tsp f =
  Storage_reader.get_network @@ fun network ->
  let time_between_blocks = match network with
    | Testnet -> 30000.
    | _ -> 60000. in
  Mrequest.ENode.get_node_base_url network @@ fun base ->
  async @@ fun () ->
  Mrequest.Node.get_header ~base () >>= function
  | Error _ -> return_unit
  | Ok header ->
    let block_tsp = header.Dune.Types.header_shell.Dune.Types.shell_timestamp in
    return (f (new%js date_fromTimeValue (date##parse (string (Dune.Types.Date.to_string block_tsp))))
              (new%js date_now) time_between_blocks)

let callback_handler ?callback ?refresh notif =
  Storage_writer.remove_notif
    ~callback:(fun fo_acc ->
        update_badge () ;
        begin match refresh with
          | Some rf -> rf fo_acc
          | None -> ()
        end ;
        match callback with
        | None -> ()
        | Some cb -> cb notif
      )
    notif

let is_approved (port : Utils.Runtime.port t) req f = match Optdef.to_option port##.sender with
  | Some sender ->
    begin match Optdef.to_option sender##.url with
      | Some url ->
        begin match Url.url_of_string (to_string url) with
          | None -> send_error port req ["Can't find sender's url"]
          | Some url ->
            let hostname = match url with
              | Url.(Http { hu_host; _ })
              | Url.(Https { hu_host; _ }) -> hu_host
              | Url.File _ -> "" in
            Storage_reader.is_approved (string hostname) f
        end
      | None -> send_error port req ["Can't find sender's url"]
    end
  | None -> send_error port req ["Can't find sender"]

let wrap_approved ?position ?(need_approve=true) port (req : _ request t) f =
  if need_approve then
    let id = to_string req##.id in
    is_approved port req
      (fun b ->
         if b then f ()
         else begin
           let should_popup =
             Optdef.case req##.metadata
               (fun () -> false)
               (fun data ->
                  if List.exists (fun (_, d) -> data = d) !pending_approved then
                    false
                  else
                    begin
                      pending_approved := (id, data) :: !pending_approved ;
                      true
                    end) in
           if should_popup then
             (js_log req ;
              Notification.show_popup_action ?position
                ~id
                port
                (fun () ->
                   pending_approved := List.remove_assoc id !pending_approved ;
                   is_approved
                     port req
                     (fun b ->
                        if b then f ()
                        else send_error port req ["It seems the user refused to approve this domain"]))
                "approve")
         end)
  else
    f ()

let wrap_unlocked ?position ?need_approve port req f =
  Storage_reader.is_unlocked (fun b ->
      if b then wrap_approved ?position ?need_approve port req f
      else
        Notification.show_popup_action ?position
          port
          (fun () ->
             Storage_reader.is_unlocked (fun b ->
                 if b then wrap_approved ?position ?need_approve port req f
                 else send_error port req ["It seems the user refused to unlock metal"])) "unlock")

let wrap_enabled ?position ?need_approve port req f =
  Storage_reader.is_enabled (fun b ->
      if b then wrap_unlocked ?position ?need_approve port req f
      else
        Notification.show_popup_action ?position
          port
          (fun () ->
             Storage_reader.is_enabled (fun b ->
                 if b then wrap_unlocked ?position port req f
                 else send_error port req ["It seems the user refused to enable metal"]))
          "enable")

let wrap_handler ?position ?need_approve port req f =
  wrap_enabled ?position ?need_approve port req f

let wrap_only_enabled ?position port req f =
  Storage_reader.is_enabled (fun b ->
      if b then wrap_unlocked ?position port req f
      else
        Notification.show_popup_action ?position
          port
          (fun () ->
             Storage_reader.is_enabled (fun b ->
                 if b then wrap_unlocked ?position port req f
                 else send_error port req ["It seems the user refused to enable metal"]))
          "enable")

let remove_window wid =
  Windows.getAll @@ fun wins ->
  List.iter (fun win ->
      match Optdef.to_option win##.id with
      | Some id when id = wid -> Windows.remove wid
      | _ -> ()) wins

let check_exist ?field f x =
  try f x with _ ->
    let msg = match field with None -> "missing field" | Some s -> "missing field \"" ^ s ^ "\"" in
    Error [ msg ]
let check_list l = List.fold_left (fun acc x -> match acc, x with
    | Error e, Ok () | Ok (), Error e -> Error e
    | Error e , Error e2 -> Error (e @ e2)
    | Ok (), Ok () -> Ok ()) (Ok ()) l
let check_array f a = check_list @@ List.map f @@ Array.to_list @@ to_array a
let check_optdef f x = match Optdef.to_option x with
  | None -> Ok ()
  | Some x -> f x
let check_pkh s =
  let s = to_string s in
  if MCrypto.check_pkh s then Ok () else Error [s ^ " is not a pkh"]
let check_int64 s =
  let s = to_string s in
  match Int64.of_string_opt s with
  | None -> Error ["cannot read int64 " ^ s]
  | Some _ -> Ok ()
let check_float s =
  let s = to_string s in
  match Float.of_string_opt s with
  | None -> Error ["cannot read float " ^ s]
  | Some _ -> Ok ()
let check_amount (s, currency) =
  let currency = to_optdef (fun s -> String.lowercase_ascii @@ to_string s) currency in
  let s2 = String.lowercase_ascii @@ to_string s in
  if s2 = "max" || s2 = "maximum" then Ok ()
  else match currency with
    | Some "dun" -> check_float s
    | Some "udun" | Some "mudun" | None -> check_int64 s
    | Some s -> Error [Printf.sprintf "currency %S not handled (only \"dun\" or \"udun\")" s]
let check_z s =
  let s = to_string s in
  try ignore @@ Z.of_string s; Ok () with _ -> Error ["cannot read zarith " ^ s]
let check_script_expr ?contract s =
  let s = to_string s in
  match Dune.parse_script ?contract s with
  | Ok _ -> Ok ()
  | Error _ -> Error ["cannot decode script " ^ s]
let check_parameters (o: parameters_js t) =
  check_optdef check_script_expr o##.value
let check_enum l ls =
  match List.fold_left2 (fun acc x s -> match acc, Optdef.to_option x with
      | None, Some _ -> Some (Ok s)
      | Some x, None -> Some x
      | None, None -> None
      | Some (Ok s1), Some _ -> Some (Error [s1; s])
      | Some (Error ls), Some _ -> Some (Error (s :: ls))) None l ls with
  | None -> Error ["none of expected field: " ^ (String.concat ", " ls)]
  | Some (Error l) -> Error ["expected only one field among: " ^ (String.concat ", " l)]
  | Some (Ok _) -> Ok ()
let check_script (o: script_js t) =
  check_list [
    check_optdef (check_script_expr ~contract:true) o##.code;
    check_exist ~field:"sc_storage" (check_script_expr ~contract:true) o##.storage;
    check_enum [ o##.code; o##.codeHash ] ["code"; "code_hash" ]
  ]
let check_notif_manager_info (o: notif_manager_info_js t) =
  check_list [
    check_optdef check_int64 o##.fee;
    check_optdef check_z o##.gasLimit;
    check_optdef check_z o##.storageLimit ]
let check_transaction_details (o: transaction_details_js t) =
  check_list [
    check_exist ~field:"dst" check_pkh o##.destination;
    check_exist ~field:"amount" check_amount (o##.amount, o##.currency);
    check_optdef check_parameters o##.parameters ]
let check_origination_details (o: origination_details_js t) =
  check_list [
    check_exist ~field:"balance" check_amount (o##.balance, o##.currency);
    check_optdef check_pkh o##.delegate;
    check_optdef check_script o##.script ]
let check_delegation_details (o: delegation_details_js t) =
  check_optdef check_pkh o##.delegate
let check_manage_account_details (o: manage_account_details_js t) =
  check_list [
    check_optdef check_pkh o##.admin;
    check_optdef (fun l -> check_list @@ to_listf check_pkh l) o##.whiteList;
    check_optdef check_pkh o##.recovery ]
let check_kind ?kind s =
  let s = to_string s in match kind with
  | Some kind when s <> kind -> Error ["expected " ^ kind ^ " got " ^ s]
  | _ -> Ok ()
let unit_optdef x = match Optdef.to_option x with
  | None -> undefined
  | Some _ -> def ()
let check_manager_operation ?kind (o: manager_op t) =
  check_list [
    check_notif_manager_info o##.info;
    check_kind ?kind o##.kind;
    check_optdef check_transaction_details o##.transaction;
    check_optdef check_origination_details o##.origination;
    check_optdef check_delegation_details o##.delegation;
    check_optdef check_manage_account_details o##.manage_account_;
    check_enum
      [unit_optdef o##.transaction; unit_optdef o##.origination;
       unit_optdef o##.delegation; unit_optdef o##.reveal;
       unit_optdef o##.manage_account_]
      ["transaction"; "origination"; "delegation"; "reveal"; "manage_account"]
  ]

let dispatch (port : Utils.Runtime.port t) (msg : _ message t) =
  let src = to_string msg##.src in
  let req = msg##.req in
  let meth = to_string req##.name in
  let id = to_string req##.id in
  if src <> "contentscript" then
    match meth with
    | "is_enabled" ->
      Storage_reader.is_enabled (fun b -> send port req (bool b)) ;
    | "is_unlocked" ->
      Storage_reader.is_enabled (fun b ->
          if b then
            Storage_reader.is_unlocked (fun b -> send port req (bool b))
          else
            send port req (bool b))
    | "is_approved" ->
      Storage_reader.is_enabled (fun b ->
          if b then
            Storage_reader.is_unlocked (fun b ->
                if b then
                  is_approved port req (fun b -> send port req (bool b))
                else send port req (bool b))
          else
            send port req (bool b))
    | "get_account" ->
      let position = match Optdef.to_option req##.data with
        | None -> None
        | Some data -> Metal_message.get_position data##.position in
      let f () =
        Storage_reader.get_account (fun acc _ -> match acc with
            | None -> send_error port req ["NO ACCOUNT ENABLED"]
            (* Error gestion ?, this should not happen because
               the extension can't be enabled without an account *)
            | Some acc -> send port req @@ string acc.pkh) in
      wrap_handler ?position port req f
    | "get_network" ->
      let position = match Optdef.to_option req##.data with
        | None -> None
        | Some data -> Metal_message.get_position data##.position in
      let f () =
        Storage_reader.get_network (fun net ->
            Mrequest.ENode.get_node_base_url net
              ~error:(fun code content ->
                  send ~ok:false port req (object%js
                    val code = code
                    val content = optdef string content end))
              (fun (EzAPI.TYPES.BASE url) ->
                 let res = object%js
                   val name = string @@ Mhelpers.network_to_string net
                   val url = string url
                 end in
                 send port req res)) in
      wrap_handler ?position port req f
    | "get_rpc" ->
      begin match Optdef.to_option req##.data with
        | None -> send_error port req ["NO DATA GIVEN"]
        | Some data ->
          let data : rpc t = Unsafe.coerce data in
          let position = Metal_message.get_position data##.position in
          match to_optdef to_string data##.rpc with
          | None -> send_error port req ["NO RPC GIVEN"]
          | Some rpc ->
            let aux f = match to_optdef to_string data##.network with
              | None -> Storage_reader.get_network f
              | Some net -> f (Mhelpers.network_of_string net) in
            let f () =
              aux @@ fun net ->
              Mrequest.ENode.get_node_base_url net @@ fun base ->
              Lwt.async (fun () ->
                  Lwt.bind (Mrequest.Node.get_raw_rpc ~base rpc) (function
                      | Error e -> let code, content = error_content0 e in
                        Lwt.return @@ send ~ok:false port req (object%js
                          val code = code
                          val content = string content end)
                      | Ok s ->
                        Lwt.return @@ send port req (_JSON##parse (string s)))) in
            wrap_handler ?position port req f end
    | "send" | "originate" | "delegate" ->
      Optdef.case req##.data
        (fun () -> send_error port req [ "EMPTY DATA" ])
        (fun data ->
           let data : op t = Unsafe.coerce data in
           let position = Metal_message.get_position data##.position in
           match List.assoc_opt id !pending_op with
           | Some _ -> send_error port req [ "Request ID already used" ]
           | None ->
             let f () =
               match check_manager_operation data##.op with
               | Ok () ->
                 pending_op := (id, data) :: !pending_op;
                 Notification.show_popup_action ?position ~id port (fun () -> ()) meth
               | Error errs -> send_error port req errs
             in
             Hashtbl.add port_table id port;
             wrap_handler ?position port req f
        )
    | "batch" ->
      Optdef.case
        (msg##.req##.data)
        (fun () -> send_error port req [ "EMPTY DATA" ])
        (fun data ->
           let data : batch t = Unsafe.coerce data in
           let position = Metal_message.get_position data##.position in
           match List.assoc_opt id !pending_batch with
           | Some _ -> send_error port req [ "Request ID already used" ]
           | None ->
             let f () =
               match check_array check_manager_operation data##.ops with
               | Ok () ->
                 pending_batch := (id, data) :: !pending_batch;
                 Notification.show_popup_action ?position ~id port (fun () -> ()) meth
               | Error errs ->
                 send_error port req errs
             in
             Hashtbl.add port_table id port;
             wrap_handler ?position port req f
        )
    | "commit_batch" ->
      begin match Optdef.to_option msg##.req##.data with
        | None -> send_error port req [ "EMPTY DATA" ]
        | Some data ->
          let data : op_config t = Unsafe.coerce data in
          let position = Metal_message.get_position data##.position in
          match List.assoc_opt id !pending_config with
          | Some _ -> send_error port req  [ "Request ID already used" ]
          | None ->
            begin match !store_batch with
              | [] -> send_error port req  [ "Batch store is empty" ]
              | _ ->
                let f () =
                  pending_config := (id, data) :: !pending_config;
                  Notification.show_popup_action ?position ~id port (fun () -> ()) meth in
                Hashtbl.add port_table id port;
                wrap_handler ?position port req f
            end
      end
    | _ -> failwith ("[Background|dispatch] Don't know what to do with : " ^ meth)
  else
    raise_js_error @@
    (new%js error_constr
      (string @@ "[Background|dispatch] query with wrong source: " ^ src))

let batch_loop_id : Dom_html.timeout_id option ref = ref None

let batch_loop time =
  (match !batch_loop_id with
   | None -> ()
   | Some id -> Dom_html.window##clearTimeout id);
  Storage_reader.get_config @@ function
  | {batch_notif = true; notif_timeout; _} ->
    let port : Utils.Runtime.port t = Unsafe.obj [||] in
    let id = "batch_store" in
    let req = Metal_message.mk_request ~name:"loop_batch" (string id) in
    let f () =
      get_last_tsp (fun block_tsp now_tsp time_between_blocks ->
          let diff = now_tsp##getTime -. block_tsp##getTime in
          let timeout = mod_float
              (2. *. time_between_blocks -. diff -. notif_timeout)
              time_between_blocks in
          ignore @@ Dom_html.window##setTimeout (wrap_callback (fun () ->
              Notification.show_popup_action ~id port (fun () -> ()) "loop_batch"
            )) timeout) in
    let f () =
      Hashtbl.add port_table id port;
      wrap_handler ~need_approve:false port req f in
    batch_loop_id := Some (Dom_html.window##setTimeout (wrap_callback f) (float_of_int (time * 1000)))
  | _ -> ()


let dispatch_popup port (msg : _ message t) =
  let src = to_string msg##.src in
  let req = msg##.req in
  let id = to_string req##.id in
  let meth = to_string req##.name in
  let get_pending l = match List.assoc_opt id l with
    | Some data -> send port req data
    | _ -> send port req @@ error_of_string "Can't find origin request" in
  let get_port f =
    match Hashtbl.find_opt port_table @@ to_string req##.id with
    | None -> log "Port disconnected for request %s" @@ to_string req##.id
    | Some port -> f port in
  if src = "popup" then
    match meth with
    | "get_metadata" -> get_pending !pending_approved
    | "get_opdata" -> get_pending !pending_op
    | "get_batchdata" -> get_pending !pending_batch
    | "get_batch_store" ->
      let network, position = match List.assoc_opt id !pending_config with
        | Some config -> config##.network, config##.position
        | None -> undefined, undefined in
      let data : batch t = object%js
        val network = network
        val position = position
        val type_ = def (string "commit")
        val ops = of_list !store_batch
      end in
      send port req data
    | "empty_batch_store" ->
      store_batch := [];
      update_badge () ;
      begin match Optdef.to_option req##.data with
        | Some data ->
          let data : _ commit_callback_msg t = Unsafe.coerce data in
          let res = data##.commit_result_ in
          remove_window data##.wid;
          get_port (fun port -> send ~ok:(to_bool res##.ok) port req res##.msg)
        | _ -> () end
    | "state_changed" -> state_changed ?data:(Optdef.to_option req##.data) ()
    | "callback_error_notif" ->
      begin match Optdef.to_option req##.data with
        | Some data ->
          Storage_writer.get_notifs @@ fun ns ->
          List.iter (fun { notifs ; _ } ->
              List.iter (fun notif ->
                  if Mhelpers.notif_id_of_not notif = to_string req##.id then
                    callback_handler ~callback:(fun _ ->
                        get_port (fun port -> send ~ok:false port req data))
                      notif)
                notifs)
            ns
        | _ -> ()
      end
    | "callback_notif" ->
      begin match Optdef.to_option req##.data with
        | Some data ->
          let data : _ notif_callback_msg t = Unsafe.coerce data in
          (* Close open notif popup *)
          Storage_writer.get_notifs @@ fun ns ->
          List.iter (fun { notifs ; _ } ->
              List.iter (function
                  | ApprovNot n as notif ->
                    if n.not_approv_id = to_string req##.id then
                      callback_handler
                        ~callback:(fun _ ->
                            let f () =
                              pending_approved := List.remove_assoc id !pending_approved;
                              remove_window n.not_approv_wid in
                            if to_bool data##.ok then
                              Storage_writer.add_approved (string n.not_approv_url) f
                            else (
                              f ();
                              get_port (fun port -> send ~ok:false port req data##.msg))
                          )
                        notif
                  | notif ->
                    if Mhelpers.notif_id_of_not notif = to_string req##.id then
                      callback_handler
                        ~callback:(fun _ ->
                            get_port (fun port -> send ~ok:(to_bool data##.ok) port req data##.msg);
                            remove_window @@ Mhelpers.notif_wid_of_not notif)
                        notif)
                notifs)
            ns
        | _ -> () end
    | "callback_add_to_batch" ->
      begin match Optdef.to_option req##.data with
        | Some d ->
          let d : add_to_batch t = Unsafe.coerce d in
          (match Optdef.to_option d##.notif##.op, Optdef.to_option d##.notif##.batch with
           | Some op, _ ->
             store_batch := !store_batch @ [ op ]
           | _, Some ops ->
             store_batch := !store_batch @ (to_list ops)
           | _ -> ());
          let notif = Of_js.notif d##.notif in
          callback_handler
            ~callback:(fun _ ->
                get_port (fun port -> send port req (string "added to batch"));
                remove_window @@ Mhelpers.notif_wid_of_not notif)
            notif;
          batch_loop d##.time
        | _ -> () end
    | "callback_postpone" ->
      begin match Optdef.to_option req##.data with
        | Some d ->
          let d : postpone t = Unsafe.coerce d in
          batch_loop d##.time;
          remove_window d##.wid
        | _ -> () end


    | _ -> failwith ("[Background|dispatch_popup] Don't know what to do with : " ^ meth)
  else
    raise_js_error @@
    (new%js error_constr
      (string @@ "[Background|dispatch_popup] query with wrong source: " ^ src))

let handle (port : Utils.Runtime.port t) (msg : _ message t) =
  log_str @@ "[Background] recv request \"" ^ (to_string port##.name) ^ "\"" ;
  js_log msg ;
  match to_string port##.name with
  | "popup" -> dispatch_popup port msg
  | "contentscript" -> dispatch port msg
  | _ -> log_str "No handler for connection with this name"
