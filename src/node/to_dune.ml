open Dune_types
open Metal_types
open MLwt

let script_to_json ?contract s = match Dune.json_of_string ?contract s with
  | Error _ -> s
  | Ok s -> s

let dune_op_base ~src {mo_det; mo_info} =
  let fee = unopt 0L mo_info.not_mi_fee in
  let gas_limit = unopt Z.zero mo_info.not_mi_gas_limit in
  let storage_limit = unopt Z.zero mo_info.not_mi_storage_limit in
  let counter = Z.zero in
  match mo_det with
  | TraDetails trd ->
    NTransaction {
      node_tr_src = src;
      node_tr_fee = fee;
      node_tr_counter = counter;
      node_tr_gas_limit = gas_limit;
      node_tr_storage_limit = storage_limit;
      node_tr_amount = trd.trd_amount;
      node_tr_dst = trd.trd_dst;
      node_tr_collect_fee_gas = if trd.trd_collect_call then Some (Z.of_int 100000) else None;
      node_tr_collect_pk = None;
      node_tr_entrypoint = unoptf None fst trd.trd_parameters;
      node_tr_parameters =
        unoptf None (fun (_e, v) ->
            convopt script_to_json v)
          trd.trd_parameters;
      node_tr_metadata = None;
    }
  | OriDetails ord ->
    NOrigination {
      node_or_src = src;
      node_or_fee = fee;
      node_or_counter = counter;
      node_or_gas_limit = gas_limit;
      node_or_storage_limit = storage_limit;
      node_or_balance = ord.ord_balance;
      node_or_script = convopt (fun (sc_code, sc_storage, sc_code_hash) ->
          let sc_code = convopt (script_to_json ~contract:true) sc_code in
          let sc_storage = script_to_json sc_storage in
          {sc_code; sc_storage; sc_code_hash}) ord.ord_script;
      node_or_metadata = None ;
    }
  | DelDetails del ->
    NDelegation {
      node_del_src = src;
      node_del_fee = fee;
      node_del_counter = counter;
      node_del_gas_limit = gas_limit;
      node_del_storage_limit = storage_limit;
      node_del_delegate = del;
      node_del_metadata = None;
    }
  | RvlDetails pk ->
    NReveal {
      node_rvl_src = src;
      node_rvl_fee = fee;
      node_rvl_counter = counter;
      node_rvl_gas_limit = gas_limit;
      node_rvl_storage_limit = storage_limit;
      node_rvl_pubkey = pk;
      node_rvl_metadata = None;
    }
  | ManDetails (_target, options) ->
    let node_mac_options = match options with
      | MadJSON json -> MaoJSON json
      | MadDecoded mao -> MaoDecoded {
          node_mao_maxrolls = mao.mao_maxrolls;
          node_mao_admin = mao.mao_admin;
          node_mao_white_list = mao.mao_white_list;
          node_mao_delegation = mao.mao_delegation;
          node_mao_recovery = mao.mao_recovery;
          node_mao_actions = mao.mao_actions;
        } in
    NManage_account {
      node_mac_src = src;
      node_mac_fee = fee;
      node_mac_counter = counter;
      node_mac_gas_limit = gas_limit;
      node_mac_storage_limit = storage_limit;
      node_mac_target = None;
      node_mac_metadata = None;
      node_mac_options;
    }

let dune_op ?sign_target ~src {mo_det; mo_info} =
  match dune_op_base ~src {mo_det; mo_info}, mo_det with
  | NManage_account mac, ManDetails (target, _options) ->
    (match sign_target with
     | Some sign -> sign mac.node_mac_options target
     | _ -> return (Ok None)) >>|? fun node_mac_target ->
    NManage_account {mac with node_mac_target}
  | op, _ -> return (Ok op)

let dune_ops ?sign_target ~src ops =
  map_res_s (dune_op ?sign_target ~src) ops
