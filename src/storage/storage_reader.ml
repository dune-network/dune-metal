(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Metal_types
open Storage_utils
open Ezjs_min
open Of_js
open MMisc

let get_network f =
  Chrome.Storage.get Chrome.sync (fun (o:network_entry t) -> f (network_entry o))
let get_selected f =
  Chrome.Storage.get Chrome.sync (fun (o:selected_entry t) -> f (selected_entry o))
let set_selected s = Chrome.Storage.set Chrome.sync (To_js.selected_entry s)

(* get only JS, not translation *)

let get_accounts_js f =
  Chrome.Storage.get Chrome.sync @@ fun (o:accounts_entry t) ->
  let accounts = accounts_entry o in
  f accounts

let get_accounts_full_js f =
  Chrome.Storage.get Chrome.sync @@ fun (o:accounts_entry t) ->
  let accounts = accounts_entry o in
  Chrome.Storage.get Chrome.Storage.local @@ fun (h:accounts_history_entry t) ->
  f @@ merge_accounts_history accounts h

let get_notifs_js f =
  Chrome.Storage.get Chrome.Storage.local (fun (n : notif_entry t) -> f n##.notifs)

let get_account_aux_js g f =
  g @@ fun accs_js ->
  let accs = to_list accs_js in
  let first () =
    let acc = List.nth_opt accs 0 in
    set_selected (convopt (fun x -> to_string x##.pkh) acc);
    f (Optdef.option acc) accs_js in
  get_selected @@ function
  | None -> first ()
  | Some s ->
    match List.find_opt (fun x -> x##.pkh = string s) accs with
    | None -> first ()
    | acc -> f (Optdef.option acc) accs_js

let get_account_js f = get_account_aux_js get_accounts_js f
let get_account_full_js f = get_account_aux_js get_accounts_full_js f

let get_custom_networks_js f =
  Chrome.Storage.get Chrome.sync @@ fun (o:custom_networks_entry t) ->
  match Optdef.to_option o##.custom_networks with
  | None -> f (array [||])
  | Some a -> f a

(* translate to ml object *)

let get_accounts f =
  Chrome.Storage.get Chrome.sync @@ fun (o:accounts_entry t) ->
  let accounts = accounts_entry o in
  Chrome.Storage.get Chrome.Storage.local @@ fun (h:accounts_history_entry t) ->
  f @@ accounts_full accounts h

let get_account f =
  get_accounts @@ fun l ->
  let first () =
    let acc = List.nth_opt l 0 in
    set_selected (convopt (fun x -> x.pkh) acc);
    f acc l in
  get_selected @@ function
  | None -> first ()
  | Some selected ->
    match List.find_opt (fun {pkh; _} -> pkh = selected) l with
    | None -> first ()
    | acc -> f acc l

let get_notifs f =
  Chrome.Storage.get Chrome.Storage.local (fun (n : notif_entry t) -> f (notifs_entry n))

let get_custom_networks f =
  Chrome.Storage.get Chrome.sync (fun (o:custom_networks_entry t) ->
      f (custom_networks_entry o))

let get_approved f =
  Chrome.Storage.get Chrome.sync (fun (o:approved_entry t) ->
      let app = unoptdef_f [||] to_array o##.approved in
      f (Array.to_list app))

let is_approved name callback =
  get_approved (fun approved ->
      callback @@ List.exists (fun n -> n = name) approved)

let get_state f =
  Chrome.Storage.get Chrome.sync (fun (o:state_entry t) -> f o##.state)

let is_unlocked callback =
  Vault.Cb.is_locked (fun b -> callback @@ not b)

let is_enabled callback =
  get_accounts_js (fun accs -> callback (accs##.length <> 0))

let get_keys () =
  Chrome.Storage.get Chrome.sync js_log

let get_config f =
  Chrome.Storage.get Chrome.sync (fun (o: config_entry t) -> f (config o))
