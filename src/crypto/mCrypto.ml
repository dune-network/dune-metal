(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

module Pbkdf = Pbkdf
module Bip39 = Bip39
module Box = Box

module Base58 = struct
  let remove_prefix ~prefix s =
    let x = String.length prefix in
    let n = String.length s in
    String.sub s x (n - x)

  let has_prefix ~prefix s =
    let x = String.length prefix in
    let n = String.length s in
    n >= x && String.sub s 0 x = prefix

  let common_prefix s1 s2 =
    let last = min (String.length s1) (String.length s2) in
    let rec loop i =
      if last <= i then last
      else if s1.[i] = s2.[i] then
        loop (i+1)
      else
        i in
    loop 0

  let may_cons xs x = match x with None -> xs | Some x -> x :: xs

  let filter_map f l =
    List.rev @@ List.fold_left (fun acc x -> may_cons acc (f x)) [] l

  let base = 58
  let zbase = Z.of_int base

  let log2 x = log x /. log 2.
  let log2_base = log2 (float_of_int base)


  module Alphabet = struct

    type t = { encode: string ; decode: string }

    let make alphabet =
      if String.length alphabet <> base then
        invalid_arg "Base58: invalid alphabet (length)" ;
      let str = Bigstring.make 256 '\255' in
      for i = 0 to String.length alphabet - 1 do
        let char = int_of_char alphabet.[i] in
        if Bigstring.get str char <> '\255' then
          Format.kasprintf invalid_arg
            "Base58: invalid alphabet (dup '%c' %d %d)"
            (char_of_int char) (int_of_char @@ Bigstring.get str char) i ;
        Bigstring.set str char (char_of_int i) ;
      done ;
      { encode = alphabet ; decode = Bigstring.to_string str }

    let bitcoin =
      make "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"
    let ripple =
      make "rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz"
    let flickr =
      make "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ"

    let default = bitcoin

    let all_in_alphabet alphabet string =
      let ok = Array.make 256 false in
      String.iter (fun x -> ok.(Char.code x) <- true) alphabet.encode ;
      let res = ref true in
      for i = 0 to (String.length string) - 1 do
        res := !res && ok.(Char.code string.[i])
      done;
      !res

    let pp ppf { encode; _ } = Format.fprintf ppf "%s" encode

  end

  open Alphabet

  let count_trailing_char s c =
    let len = String.length s in
    let rec loop i =
      if i < 0 then len
      else if String.get s i <> c then (len-i-1)
      else loop (i-1) in
    loop (len-1)

  let count_leading_char s c =
    let len = String.length s in
    let rec loop i =
      if i = len then len
      else if String.get s i <> c then i
      else loop (i+1) in
    loop 0

  let of_char ?(alphabet=Alphabet.default) x =
    let pos = String.get alphabet.decode (int_of_char x) in
    if pos = '\255' then failwith (Printf.sprintf "Invalid data %c" x);
    int_of_char pos

  let to_char ?(alphabet=Alphabet.default) x =
    alphabet.encode.[x]

  let raw_encode ?(alphabet=Alphabet.default) s =
    let len = String.length s in
    let s = String.init len (fun i -> String.get s (len - i - 1)) in
    let zero = alphabet.encode.[0] in
    let zeros = count_trailing_char s '\000' in
    let res_len = (len * 8 + 4) / 5 in
    let res = Bytes.make res_len '\000' in
    let s = Z.of_bits s in
    let rec loop s =
      if s = Z.zero then 0 else
        let s, r = Z.div_rem s zbase in
        let i = loop s in
        Bytes.set res i (to_char ~alphabet (Z.to_int r)) ;
        i + 1 in
    let i = loop s in
    let res = Bytes.sub_string res 0 i in
    String.make zeros zero ^ res

  let raw_decode ?(alphabet=Alphabet.default) s =
    let zero = alphabet.encode.[0] in
    let zeros = count_leading_char s zero in
    let len = String.length s in
    let rec loop res i =
      if i = len then res else
        let x = Z.of_int (of_char ~alphabet (String.get s i)) in
        let res = Z.(add x (mul res zbase)) in
        loop res (i+1)
    in
    let res = Z.to_bits @@ loop Z.zero zeros in
    let res_tzeros = count_trailing_char res '\000' in
    let len = String.length res - res_tzeros in
    String.make zeros '\000' ^
    String.init len (fun i -> String.get res (len - i - 1))

  let checksum s =
    let hash =
      Digestif.SHA256.digest_string @@
      Digestif.SHA256.to_raw_string @@
      Digestif.SHA256.digest_string @@
      s in
    let res = Digestif.SHA256.to_raw_string hash in
    String.sub res 0 4

  (* Append a 4-bytes cryptographic checksum before encoding string s *)
  let safe_encode ?alphabet s =
    raw_encode ?alphabet (s ^ checksum s)

  let safe_decode ?alphabet s =
    let s = raw_decode ?alphabet s in
    let len = String.length s in
    let msg = String.sub s 0 (len-4)
    and msg_hash = String.sub s (len-4) 4 in
    if msg_hash <> checksum msg then
      invalid_arg "safe_decode" ;
    msg

  let decode ?alphabet prefix s =
    Bigstring.of_string @@ remove_prefix ~prefix @@ safe_decode ?alphabet s

  let encode ?alphabet prefix b =
    safe_encode ?alphabet (prefix ^ Bigstring.to_string b)

end

module Prefix = struct

  (* 32 *)
  let block_hash = "\001\052" (* B(51) *)
  let operation_hash = "\005\116" (* o(51) *)
  let operation_list_hash = "\133\233" (* Lo(52) *)
  let operation_list_list_hash = "\029\159\109" (* LLo(53) *)
  let protocol_hash = "\002\170" (* P(51) *)
  let context_hash = "\079\199" (* Co(52) *)
  let script_expr_hash = "\013\044\064\027" (* expr(54) *)

  (* 20 *)
  let ed25519_public_key_hash = "\004\177\001" (* dn1(36) *)
  let secp256k1_public_key_hash = "\004\177\003" (* dn2(36) *)
  let p256_public_key_hash = "\004\177\006" (* dn3(36) *)
  let ed25519_public_key_hash_tz = "\006\161\159" (* tz1(36) *)
  let secp256k1_public_key_hash_tz = "\006\161\161" (* tz2(36) *)
  let p256_public_key_hash_tz = "\006\161\164" (* tz3(36) *)
  let contract_public_key_hash = "\002\090\121" (* KT1(36) *)

  let pkh s =
    if String.length s < 3 then None
    else match String.sub s 0 3 with
      | "dn1" -> Some ed25519_public_key_hash
      | "dn2" -> Some secp256k1_public_key_hash
      | "dn3" -> Some p256_public_key_hash
      | "KT1" -> Some contract_public_key_hash
      | _ -> None

  (* 16 *)
  let cryptobox_public_key_hash = "\153\103" (* id(30) *)

  (* 32 *)
  let ed25519_seed = "\013\015\058\007" (* edsk(54) *)
  let ed25519_public_key = "\013\015\037\217" (* edpk(54) *)
  let secp256k1_secret_key = "\017\162\224\201" (* spsk(54) *)
  let p256_secret_key = "\016\081\238\189" (* p2sk(54) *)

  (* 56 *)
  let ed25519_encrypted_seed = "\007\090\060\179\041" (* edesk(88) *)
  let secp256k1_encrypted_secret_key = "\009\237\241\174\150" (* spesk(88) *)
  let p256_encrypted_secret_key = "\009\048\057\115\171" (* p2esk(88) *)

  (* 33 *)
  let secp256k1_public_key = "\003\254\226\086" (* sppk(55) *)
  let p256_public_key = "\003\178\139\127" (* p2pk(55) *)
  let secp256k1_scalar = "\038\248\136" (* SSp(53) *)
  let secp256k1_element = "\005\092\000" (* GSp(54) *)

  let pk s =
    if String.length s < 4 then None
    else match String.sub s 0 4 with
      | "edpk" -> Some ed25519_public_key
      | "sppk" -> Some secp256k1_public_key
      | "p2pk" -> Some p256_public_key
      | _ -> None

  (* 64 *)
  let ed25519_secret_key = "\043\246\078\007" (* edsk(98) *)
  let ed25519_signature = "\009\245\205\134\018" (* edsig(99) *)
  let secp256k1_signature =  "\013\115\101\019\063" (* spsig1(99) *)
  let p256_signature =  "\054\240\044\052" (* p2sig(98) *)
  let generic_signature = "\004\130\043" (* sig(96) *)

  let sk ?(only_ed25519=true) s =
    if String.length s < 4 then None
    else match String.sub s 0 4 with
      | "edsk" ->
        if String.length s = 54 then Some (ed25519_seed, true)
        else if String.length s = 98 then Some (ed25519_secret_key, false)
        else None
      | "spsk" when not only_ed25519 -> Some (secp256k1_secret_key, true)
      | "p2sk" when not only_ed25519 -> Some (p256_secret_key, true)
      | _ -> None

  let csig s =
    let n = String.length s in
    if n = 96 && String.sub s 0 3 = "sig" then Some generic_signature
    else if n = 98 && String.sub s 0 5 = "p2sig" then Some p256_signature
    else if n = 99 && String.sub s 0 5 = "edsig" then Some ed25519_signature
    else if n = 99 && String.sub s 0 6 = "spsig1" then Some secp256k1_signature
    else None

  (* 4 *)
  let chain_id = "\087\082\000" (* Net(15) *)

end

module Blake2b_20 = struct
  module Blake2b = Digestif.Make_BLAKE2B(struct let digest_size = 20 end)

  let hash_bytes ?_key l =
    Bigstring.of_string @@ Blake2b.to_raw_string @@ Blake2b.get @@
    List.fold_left (fun state b -> Blake2b.feed_bigstring state b)
      (Blake2b.init ()) l

  let hash_string ?_key l =
    Bigstring.of_string @@ Blake2b.to_raw_string @@ Blake2b.get @@
    List.fold_left (fun state b -> Blake2b.feed_string state b)
      (Blake2b.init ()) l
end

module Blake2b_32 = struct
  module Blake2b = Digestif.Make_BLAKE2B(struct let digest_size = 32 end)

  let hash_bytes ?_key l =
    Bigstring.of_string @@ Blake2b.to_raw_string @@ Blake2b.get @@
    List.fold_left (fun state b -> Blake2b.feed_bigstring state b)
      (Blake2b.init ()) l

  let hash_string ?_key l =
    Bigstring.of_string @@ Blake2b.to_raw_string @@ Blake2b.get @@
    List.fold_left (fun state b -> Blake2b.feed_string state b)
      (Blake2b.init ()) l
end

module Pkh = struct
  let b58enc ?alphabet ?(prefix=Prefix.ed25519_public_key_hash) b =
    Base58.encode ?alphabet prefix b
  let b58dec ?alphabet s =
    match Prefix.pkh s with
    | Some prefix -> Base58.decode ?alphabet prefix s
    | None -> assert false
end

module Pk = struct
  let b58enc ?alphabet ?prefix b =
    let prefix =
      if Bigstring.length b = 32 then Prefix.ed25519_public_key
      else match prefix with None -> Prefix.secp256k1_public_key | Some p -> p in
    Base58.encode ?alphabet prefix b
  let b58dec ?alphabet s =
    match Prefix.pk s with
    | Some prefix -> Base58.decode ?alphabet prefix s
    | None -> assert false

  let hash b =
    Blake2b_20.hash_bytes [ b ]
end

module Sk = struct
  let b58enc ?alphabet ?(prefix=Prefix.ed25519_seed) b =
    let b =
      if Bigstring.length b = 64 then Bigstring.sub b 32 32
      else b in
    Base58.encode ?alphabet prefix b

  let b58dec ?alphabet ?only_ed25519 s =
    match Prefix.sk ?only_ed25519 s with
    | Some (prefix, short) ->
      let b = Base58.decode ?alphabet prefix s in
      if short then b
      else Bigstring.sub b 32 32
    | None -> assert false

  let to_public_key b =
    Hacl.Sign.unsafe_to_bytes @@ Hacl.Sign.neuterize @@ Hacl.Sign.unsafe_sk_of_bytes b
end


module Operation_hash = struct
  type t = Bigstring.t

  let b58enc ?alphabet b =
    Base58.encode ?alphabet Prefix.operation_hash b
  let b58dec ?alphabet  s =
    Base58.decode ?alphabet Prefix.operation_hash s

  let equal = Bigstring.equal

  let hash b =
    Blake2b_32.hash_bytes [ b ]
end

let pk_to_dn1 hash = Pkh.b58enc (Blake2b_20.hash_bytes [ Pk.b58dec hash ])
let op_to_KT1 hash =
  let data = Bigstring.concat "" [ hash; Bigstring.make 4 '\000' ] in
  Base58.encode Prefix.contract_public_key_hash (Blake2b_20.hash_bytes [ data ])

let prefix_dn_tz hash =
  if String.length hash < 3 then None
  else match String.sub hash 0 3 with
    | "dn1" | "tz1" -> Some Prefix.(
        ed25519_public_key_hash, ed25519_public_key_hash_tz, String.sub hash 0 2)
    | "dn2" | "tz2" -> Some Prefix.(
        secp256k1_public_key_hash, secp256k1_public_key_hash_tz, String.sub hash 0 2)
    | "dn3" | "tz3" -> Some Prefix.(
        p256_public_key_hash, p256_public_key_hash_tz, String.sub hash 0 2)
    | _ -> None

let dn_to_tz dn = match prefix_dn_tz dn with
  | Some (dn_enc, tz_enc, "dn") ->
    Base58.(encode tz_enc (decode dn_enc dn))
  | _ -> dn

let tz_to_dn tz = match prefix_dn_tz tz with
  | Some (dn_enc, tz_enc, "tz") ->
    Base58.(encode dn_enc (decode tz_enc tz))
  | _ -> tz

let check_pkh str =
  try
    let bytes = Pkh.b58dec str in
    Bigstring.length bytes = 20
  with _ -> false

let keypair () = Hacl.Sign.keypair ()

let sk_from_seed edsk =
  let seed = Base58.decode Prefix.ed25519_seed edsk in
  let pk = Sk.to_public_key seed in
  Base58.encode Prefix.ed25519_secret_key @@ Bigstring.concat "" [pk; seed]

let sk_to_seed edsk =
  let sk = Base58.decode Prefix.ed25519_secret_key edsk in
  Base58.encode Prefix.ed25519_seed @@ Bigstring.sub sk 32 32
