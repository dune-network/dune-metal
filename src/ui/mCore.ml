(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min
open Metal_types
open Storage_utils
open MLwt
module Jstable = Js_of_ocaml.Jstable
open Chrome

let send_to_background ?cb msg =
  let info = Utils.Runtime.mk_connection_info "popup" in
  let port = Runtime.connect ~info () in
  match cb with
  | None ->
    port##postMessage msg ;
    port##disconnect
  | Some cb ->
    Utils.Browser.addListener1 port##.onMessage (fun ans ->
        cb ans;
        port##disconnect);
    port##postMessage msg

(* refresh function *)

let update_accounts (app:Vtypes.core_js t) account accounts =
  app##.account := To_js.account_full account;
  app##.accounts := of_listf To_js.account_full accounts

(* Network selector *)

let update_networks app =
  let networks = List.map To_js.network Mhelpers.network_options in
  Storage_reader.get_custom_networks @@ fun l ->
      let cnetworks = List.map (fun (name, _, _) -> string name) l in
      app##.networks := of_list (networks @ cnetworks)

(* delegate services *)

let update_bakers app =
  let state = {fo_acc = Mhelpers.dummy_account ""; fo_net = Of_js.network app##.network } in
  Mrequest.History.get_services ~state
    (fun services -> app##.bakers := of_list @@ List.rev @@ List.fold_left (fun acc s ->
         if s.srv_kind = "delegate" then
           object%js
             val name = string s.srv_name
             val pkh = string @@ unopt s.srv_name s.srv_dn1
             val logo = def @@ string s.srv_logo
           end :: acc
         else acc) [] services)

let find_baker app pkh =
  List.find_opt (fun lb -> lb##.pkh = string pkh) @@ to_list app##.bakers

(* Selected account info *)

let update_account_info ?time app =
  let cb () =
    Mrequest.ENode.get_account_info ~network:(Mhelpers.network_of_string @@ to_string app##.network)
      ~error:(fun _code _content -> app##.account_info_ := undefined)
      (to_string app##.account##.pkh) @@ fun ai ->
    app##.balance := def @@ string @@ Int64.to_string ai.Dune.Types.node_ai_balance;
    app##.delegate := optdef (fun s ->
        match find_baker app s with
        | Some baker -> baker
        | None -> object%js
          val name = string s
          val pkh = string s
          val logo = undefined
        end) ai.Dune.Types.node_ai_delegate;
    if ai.Dune.Types.node_ai_maxrolls = None && ai.Dune.Types.node_ai_admin = None &&
       ai.Dune.Types.node_ai_white_list = None && ai.Dune.Types.node_ai_delegation = None &&
       ai.Dune.Types.node_ai_recovery = None then
      app##.account_info_ := undefined
    else
      let info = object%js
        val maxrolls = Optdef.option ai.Dune.Types.node_ai_maxrolls
        val admin = optdef string ai.Dune.Types.node_ai_admin
        val whitelist = optdef (of_listf string) ai.Dune.Types.node_ai_white_list
        val delegation = optdef bool ai.Dune.Types.node_ai_delegation
        val recovery = optdef string ai.Dune.Types.node_ai_recovery
      end in
      app##.account_info_ := def info in
  match time with
  | None -> cb ()
  | Some time -> Mui.Timer.clear_create ~name:"balance" time cb

(* notifications *)

let make_batch_notif f =
  let msg = Metal_message.mk_message ~name:"get_batch_store" ~src:"popup" "batch_store" in
  let cb ans =
    if msg##.req##.id = ans##.res##.id && ans##.src = string "background" then
      let tsp = to_string (new%js date_now)##toLocaleString in
      let origin = "metal" in
      let notif_js = Dummy.notif ~tsp ~origin ~id:"batch_store" () in
      notif_js##.op := undefined;
      let batch : Metal_message_types.batch t = Unsafe.coerce ans##.res##.result in
      if batch##.ops##.length = 0 then f None
      else (
        notif_js##.batch := def batch##.ops;
        notif_js##.kind := string "batch";
        notif_js##.type_ := def (string "commit");
        f (Some (Of_js.notif notif_js))) in
  send_to_background ~cb msg

let get_notifs_account pkh notifs =
  unoptf [] (fun x -> x.notifs) @@
  List.find_opt (fun {notif_acc; _} -> notif_acc = pkh) notifs

let refresh_notifs app ns =
  let pkh = to_string app##.core##.account##.pkh in
  let ns = get_notifs_account pkh ns in
  app##.core##.notifications := of_listf To_js.notif ns ;
  match ns with
  | [] -> app##.path := string "home"
  | _ -> ()

let update_account_notifs0 ?(background=true) app =
  Storage_reader.get_notifs @@ fun ns ->
  let notifs = get_notifs_account (to_string app##.account##.pkh) ns in
  let f notifs =
    app##.notifications := of_listf To_js.notif notifs;
    Browser_action.set_badge
      ~text:(if notifs = [] then "" else string_of_int (List.length notifs)) () in
  if background then
    make_batch_notif (function
        | None -> f notifs
        | Some batch_notif -> f (batch_notif :: notifs))
  else f notifs

let update_account_notifs ?time ?background app =
  let cb () = update_account_notifs0 ?background app in
  match time with
  | None -> cb ()
  | Some time -> Mui.Timer.clear_create ~name:"notifs" time cb

let commit_callback id wid result =
  let data : _ Metal_message_types.commit_callback_msg t = object%js
    val wid = wid
    val commit_result_ = result
  end in
  let msg = Metal_message.mk_message ~data ~name:"empty_batch_store" ~src:"popup" id in
  send_to_background msg

let deny_callback n =
  let id = Mhelpers.notif_id_of_not n in
  let data : js_string Metal_message_types.notif_callback_msg t = object%js
    val ok = _false
    val msg = def (string "notification denied")
  end in
  let msg = Metal_message.mk_message ~data ~name:"callback_notif" ~src:"popup" id in
  send_to_background msg

let deny_notif app n =
  let n = Of_js.notif n in
  let id = Mhelpers.notif_id_of_not n in
  match n with
  | BatchNot {not_type = Some "commit"; _} | BatchNot {not_type = Some "loop"; _} ->
    commit_callback id (Mhelpers.notif_wid_of_not n) (
      object%js
        val ok = _false
        val msg = def @@ string "commit denied"
      end);
    update_account_notifs0 ~background:false app##.core
  | _ -> deny_callback n

let ok_callback ?msg n =
  let id = Mhelpers.notif_id_of_not n in
  let data : _ Metal_message_types.notif_callback_msg t = object%js
    val ok = _true
    val msg = Optdef.option msg
  end in
  let msg = Metal_message.mk_message ~data ~name:"callback_notif" ~src:"popup" id in
  send_to_background msg

let ok_notif n (msg : 'a t optdef) =
  let msg = Optdef.to_option msg in
  let n = Of_js.notif n in
  let id = Mhelpers.notif_id_of_not n in
  begin match n with
    | BatchNot {not_type = Some "commit"; _} ->
      commit_callback id (Mhelpers.notif_wid_of_not n) (
        object%js
          val ok = _true
          val msg = Optdef.option msg
        end)
    | _ -> ok_callback ?msg n
  end

let error_callback id (data : _ t) =
  send_to_background @@
  Metal_message.mk_message ~data ~name:"callback_error_notif" ~src:"popup" id

(* revealed *)

let get_revealed (app:Vtypes.core_js t) =
  let network = Mhelpers.network_of_string @@ to_string app##.network in
  let revealed = Of_js.network_assoc to_string app##.account##.revealed in
  List.assoc_opt network revealed

let update_reveal ?time (app:Vtypes.core_js t) =
  let cb () =
    match get_revealed app with
    | Some _ -> ()
    | None ->
      let state = Of_js.state app##.account app##.network in
      Mrequest.ENode.get_revealed ~state @@ function
      | None -> ()
      | Some hash ->
        let account = {
          state.fo_acc with
          revealed = (state.fo_net, hash) :: state.fo_acc.revealed} in
        Storage_writer.update_account ~callback:(update_accounts app) account in
  match time with
  | None -> cb ()
  | Some time -> Mui.Timer.clear_create ~name:"reveal" time cb

(* notarization utils *)

let split_on_underscores s =
  let n = String.length s in
  let rec aux i_start =
    match String.index_from_opt s i_start '_' with
    | None -> (s, None)
    | Some i ->
      if String.get s (i+1) = '_' then
        String.sub s 0 i, Some (String.sub s (i+2) (n-i-2))
      else
        aux (i+1) in
  aux 0

let fill_notarization_info app ops =
  let table = Jstable.create () in
  List.iter (fun op ->
      match List.nth_opt op.bo_op 0 with
      | Some op2 -> (match op2.mo_det with
          | TraDetails trd ->
            (match trd.trd_parameters with
             | Some (_, Some v) ->
               (match Dune.Encoding.Script.decode v with
                | Dune.(Types.Mich Micheline.Types.(Canonical (
                    Prim (_, "Pair", [Bytes (_, file_hash); Bytes (_, comments)], _)))) ->
                   let name, comments = split_on_underscores @@ Bytes.to_string comments in
                   let info = object%js
                     val file_hash_ = string Hex.(show @@ of_bytes file_hash)
                     val file_name_ = string name
                     val comments = optdef string comments
                   end in
                   Jstable.add table (string op.bo_hash) info
                 | _ -> ())
             | _ -> ())
          | _ -> ())
      | _ -> ()) ops;
  app##.notarization_info_ := table

(* operation history *)

let refresh_history app kind page =
  let kind = to_string kind in
  let cap_kind = String.capitalize_ascii kind in
  let state = Of_js.state app##.account app##.network in
  (match kind with
   | "transaction" -> app##.transaction_history_##.page := page + 1
   | "origination" -> app##.origination_history_##.page := page + 1
   | "delegation" -> app##.delegation_history_##.page := page + 1
   | "notarization" -> app##.notarization_history_##.page := page + 1
   | "manage_account" -> app##.manage_account_history_##.page := page + 1
   | _ -> ());
  let cb () =
    let page = match kind with
      | "transaction" -> app##.transaction_history_##.page - 1
      | "origination" -> app##.origination_history_##.page - 1
      | "delegation" -> app##.delegation_history_##.page - 1
      | "notarization" -> app##.notarization_history_##.page - 1
      | "manage_account" -> app##.manage_account_history_##.page - 1
      | _ -> page in
    Mrequest.History.page_wrap_simple ~page ~page_size:10 ~state ~refresh:(update_accounts app)
      (fun x -> Some [x]) cap_kind @@ fun total l ->
    let ops = of_listf To_js.block_operation l in
    match kind with
    | "transaction" ->
      app##.transaction_history_##.ops := ops;
      app##.transaction_history_##.total := total;
    | "origination" ->
      app##.origination_history_##.ops := ops;
      app##.origination_history_##.total := total
    | "delegation" ->
      app##.delegation_history_##.ops := ops;
      app##.delegation_history_##.total := total
    | "notarization" ->
      app##.notarization_history_##.ops := ops;
      app##.notarization_history_##.total := total;
      fill_notarization_info app l
    | "manage_account" ->
      app##.manage_account_history_##.ops := ops;
      app##.manage_account_history_##.total := total
    | _ -> () in
  Mui.Timer.clear_create ~name:"history" 60 cb

(* update account main *)

let fill_op_information app =
  let args = Url.Current.arguments in
  let exp = match List.assoc_opt "expanded" args with
    | Some b -> begin try bool_of_string b with _ -> false end
    | _ -> false
  in
  match List.assoc_opt "type" args with
  | Some "transaction" ->
    let tr = Dummy.trd () in
    (match List.assoc_opt "tr_dst" args with
     | Some dst -> tr##.destination := string dst
     | _ -> ());
    (match List.assoc_opt "tr_am" args with
     | Some amount -> tr##.amount := string amount
     | _ -> ());
    app##.tr_expanded_ := bool exp ;
    app##.op##.transaction := def tr;
    "transaction", 0
  | Some "origination" ->
    let ord = Dummy.ord () in
    (match List.assoc_opt "or_balance" args with
     | Some balance -> ord##.balance := string balance
     | _ -> ());
    app##.op##.origination := def ord;
    "origination", 2
  | Some "delegation" ->
    let dlg = Dummy.dlg () in
    (match List.assoc_opt "dlg_delegate" args with
     | Some delegate -> dlg##.delegate := def @@ string delegate
     | _ -> ());
    app##.op##.delegation := def dlg;
    app##.del_expanded_ := bool exp ;
    "delegation", 1
  | _ -> "transaction", 0

let update_account (app:Vtypes.core_js t) =
  update_reveal app;
  update_account_info app;
  update_account_notifs app;
  let kind, tab_index = fill_op_information app in
  app##.operation_tab_ := tab_index;
  refresh_history app (string kind) 0

(* Accounts list *)

let change_account app account =
  Storage_writer.update_selected ~callback:(fun () ->
      app##.core##.account := account;
      update_account app##.core;
      app##.path := string "home") (Some (to_string account##.pkh))

let get_import_accounts app =
  let state = Of_js.state app##.account app##.network in
  let pkh_list = to_listf (fun acc -> to_string acc##.pkh) app##.accounts in
  Mrequest.History.get_operations_page ~manager:true ~state ~refresh:(fun _ _ -> ())
    (Mhelpers.origination_opt ~failed:true) "Origination"
    (fun l_ori ->
       let kt1_list_all = filter_map (fun ori -> ori.bo_op.mo_det.ord_kt1) l_ori in
       let kt1_list = List.filter (fun kt1 -> not (List.mem kt1 pkh_list)) kt1_list_all in
       app##.import_accounts_ := of_listf string kt1_list;
       app##.importation := _true)

let clear_import app = app##.importation := _false

let import_kt1 ?callback {pkh; vault; _} fo_net kt1s = match vault with
  | Local vault ->
    let rec aux = function
      | [] -> ()
      | hd :: tl ->
        let callback = match callback, tl with Some cb, [] -> Some cb | _ -> None in
        async_req ~error:(fun code content ->
            log "error import kt1 %d %s" code (unopt "" content))
          (Vault.Lwt.decrypt ~pkh ~vault >>=? fun sk ->
           Vault.Lwt.encrypt ~manager:pkh ~pkh:hd sk)
          (fun account_kt1 ->
             Mrequest.ENode.get_account_info ~network:fo_net
               ~error:(fun _ _ ->
                   Storage_writer.add_account ?callback account_kt1 ;
                   aux tl)
               hd
               (fun acc_info ->
                  let manager_kt = match acc_info.Dune.Types.node_ai_script with
                    | Some (Some sc, _, _) -> Some (sc = Mrequest.Node.manager_kt)
                    | _ -> Some false in
                  Storage_writer.add_account ?callback { account_kt1 with manager_kt } ;
                  aux tl
               )) in
    aux kt1s
  | Ledger _ ->
    List.iteri (fun i kt1 ->
        let account_kt1 = Mhelpers.dummy_account ~vault kt1 in
        let callback = match callback with
          | Some cb when i = (List.length kt1s) - 1 -> Some cb
          | _ -> None in
        Mrequest.ENode.get_account_info ~network:fo_net
          ~error:(fun _ _ -> Storage_writer.add_account ?callback account_kt1)
          kt1
          (fun acc_info ->
             let manager_kt = match acc_info.Dune.Types.node_ai_script with
               | Some (Some sc, _, _) -> Some (sc = Mrequest.Node.manager_kt)
               | _ -> Some false in
             Storage_writer.add_account ?callback { account_kt1 with manager_kt }
          )) kt1s

let import_accounts app =
  let account = Of_js.account_full app##.account in
  let network = Of_js.network app##.network in
  let kt1s = to_listf to_string app##.selected_import_accounts_ in
  import_kt1 ~callback:(fun accs ->
      app##.accounts := of_listf To_js.account_full accs;
      app##.importation := _false)
    account network kt1s


(* Operation *)

let manage_op_result app fg =
  app##.ml_forge_result_ := fg;
  let amount = List.fold_left (fun acc op -> match op with
      | Dune.Types.NTransaction {Dune.Types.node_tr_amount; _} ->
        Int64.add acc node_tr_amount
      | Dune.Types.NOrigination {Dune.Types.node_or_balance; _} ->
        Int64.add acc node_or_balance
      | _ -> acc) 0L fg.fg_ops in
  let fee, gas_limit, storage_limit = Dune.Utils.limits_of_operations fg.fg_ops in
  let burn = Int64.mul 1000L @@ Z.to_int64 storage_limit in
  app##.op_result_ := object%js
    val amount = To_js.of_int64 amount
    val fee = To_js.of_int64 fee
    val gas_limit_ = To_js.of_z gas_limit
    val storage_limit_ = To_js.of_z storage_limit
    val burn = To_js.of_int64 burn
  end

let forge_op ?network app op =
  let op = Of_js.(manager_operation notif_manager_info op) in
  let state = Of_js.state app##.account app##.network in
  let state = {state with fo_net = unopt state.fo_net network} in
  let acc, network = state.fo_acc, state.fo_net in
  (match op.mo_det with
   | TraDetails trd ->
     begin match acc.manager_kt with
       | Some true ->
         Mrequest.ENode.get_node_base_url_lwt network >>=? fun base ->
         Mrequest.Node.manager_kt_transaction_param ~base
           trd.trd_dst trd.trd_amount (unopt (None, None) trd.trd_parameters)
         >>= fun parameters ->
         let op = {op with mo_det = TraDetails {
             trd_dst = acc.pkh;
             trd_amount = 0L;
             trd_parameters = Some (Some "do", parameters;);
             trd_collect_call = false} } in
         Mrequest.ENode.forge_manager_operations ~state [ op ]
       | _ ->
         if trd.trd_amount = Int64.minus_one then (
           Mrequest.ENode.forge_empty_account ~state trd.trd_dst)
         else (
           let op = {op with mo_det = TraDetails trd} in
           Mrequest.ENode.forge_manager_operations ~state [ op ])
     end
   | OriDetails ord ->
     let op = {op with mo_det = OriDetails ord} in
     Mrequest.ENode.forge_manager_operations ~state [ op ]
   | DelDetails delegate ->
     Mrequest.ENode.get_account_info ~network acc.pkh
       ~error:(fun _ _ -> app##.old_delegate_ := undefined)
       (fun node_account ->
          app##.old_delegate_ := optdef string node_account.Dune.Types.node_ai_delegate) ;
     begin match acc.manager_kt with
       | Some true ->
         let op = {op with mo_det = TraDetails {
             trd_amount = 0L; trd_dst = acc.pkh;
             trd_parameters = Some (
                 Some "do", Mrequest.Node.manager_kt_delegation_param delegate);
             trd_collect_call = false
           } } in
         Mrequest.ENode.forge_manager_operations ~state [ op ]
       | _ ->
         let op = {op with mo_det = DelDetails delegate} in
         Mrequest.ENode.forge_manager_operations ~state [ op ] end
   | ManDetails (_target, options) ->
     Mrequest.ENode.get_accounts_lwt () >>= fun accounts ->
     begin match Mrequest.ENode.get_admin ~state accounts with
       | Error e -> return (Error e)
       | Ok admin ->
         let acc, state, target = match admin with
           | None -> acc, state, None
           | Some admin -> admin, {state with fo_acc = admin},
                           Some (acc.pkh, Vault.Lwt.sign ~watermark:"" acc) in
         app##.manage_account_admin_ := def (string acc.pkh);
         let op = {op with mo_det = ManDetails (target, options)} in
         Mrequest.ENode.forge_manager_operations ~state [ op ] end
   | _ -> return (Error (Str_err "not implemented")))
  >>|? (manage_op_result app)

let forge_batch ?network app ops =
  let ops = to_listf Of_js.(manager_operation notif_manager_info) ops in
  let ops = List.fold_left (fun acc -> function
      | {mo_det = TraDetails trd; mo_info} -> {mo_det = TraDetails trd; mo_info} :: acc
      | {mo_det = OriDetails ord; mo_info} -> {mo_det = OriDetails ord; mo_info} :: acc
      | {mo_det = DelDetails del; mo_info} -> {mo_det = DelDetails del; mo_info} :: acc
      | {mo_det = RvlDetails rvl; mo_info} -> {mo_det = RvlDetails rvl; mo_info} :: acc
      | _ -> acc) [] ops in
  let state = Of_js.state app##.account app##.network in
  let state = {state with fo_net = unopt state.fo_net network} in
  Mrequest.ENode.forge_manager_operations ~state ops >>|?
  (manage_op_result app)

let hash_file app f =
  let hash = Digestif.SHA256.init () in
  let process result =
    match Opt.to_option (File.CoerceTo.arrayBuffer result) with
    | None -> failwith "Not a string"
    | Some ab ->
      let bs = Js_of_ocaml.Typed_array.Bigstring.of_arrayBuffer ab in
      ignore(Digestif.SHA256.feed_bigstring hash bs) in
  let finalize name =
    f Digestif.SHA256.(to_raw_string @@ get hash) name in
  Mui.wait @@ fun () ->
  match Opt.to_option app##.notarization_file_ with
  | None -> ()
  | Some f -> match Opt.to_option @@ File.CoerceTo.blob f with
    | None -> ()
    | Some f -> Mui.input_by_chunk_file f process finalize

let notarization_params hash name comments =
  let metadata = match comments with
    | None -> name
    | Some cm -> name ^ "__" ^ cm in
  let params = Dune.Types.Mich Micheline.Types.(Canonical (Prim (
      0, "Pair", [
        Bytes (0, Bytes.of_string hash);
        Bytes (0, Bytes.of_string metadata) ], []))) in
  Dune.Encoding.Script.encode params

let notarization_contract app =
  match Mhelpers.notarization_contract (Of_js.network app##.core##.network) with
  | None -> undefined
  | Some kt1 -> def @@ string kt1

let notarization_op app f =
  let core = app##.core in
  let destination = match Optdef.to_option @@ notarization_contract app with
    | Some kt1 -> kt1
    | None -> string "" in
  let comments =
    if core##.notarization_comments_ = string "" then None
    else Some (to_string core##.notarization_comments_) in
  hash_file core (fun hash name ->
      let value = notarization_params hash name comments in
      let op = object%js
        val mutable kind = string "transaction"
        val mutable info = core##.op##.info
        val mutable origination = def @@ Dummy.ord ()
        val mutable delegation = def @@ Dummy.dlg ()
        val mutable reveal = def @@ Dummy.rvl ()
        val mutable manage_account_ = def @@ Dummy.man ()
        val mutable transaction = def @@ object%js
            val mutable destination = destination
            val mutable amount = string "0"
            val mutable currency = undefined
            val mutable parameters = def @@ object%js
                val mutable entrypoint = undefined
                val mutable value = def @@ string value
              end
            val mutable collectCall = _false
          end
      end in
      f op)

let show_modal app =
  let modal : Vtypes.vue_modal t = Vue_js.get_ref app "notif-modal" in
  modal##show

let display_notif app =
  if app##.path = string "home" then show_modal app
  else app##.path := string "notif"

let show_op ?network app op =
  let aux op =
    match Check_js.(manager_operation notif_manager_info op) with
    | Error e ->
      let errors = List.map (fun e ->
          let code, content = error_content e in
          Mui.make_error code content) e in
      let errors_a = of_list errors in
      app##.core##.notif_error_ := def errors_a;
      app##.core##.forging := _false;
      error_callback (to_string app##.core##.notif##.id) errors_a;
      display_notif app
    | Ok op ->
      app##.core##.notif##.op := def op;
      display_notif app;
      async_req
        ~error:(fun code content ->
            let data = Mui.make_error code content in
            app##.core##.notif_error_ := def @@ array [| data |];
            app##.core##.forging := _false;
            error_callback (to_string app##.core##.notif##.id) data)
        (forge_op ?network app##.core op)
        (fun () -> app##.core##.forging := _false) in
  if op##.kind = string "notarization" then notarization_op app aux
  else aux op

let show_op_kind app kind postpone=
  let core = app##.core in
  core##.forging := _true;
  core##.op##.kind := kind;
  core##.notif##.kind := string "operation";
  begin if to_bool postpone
    then core##.notif##.type_ := def (string "batch")
    else core##.notif##.type_ := def (string "direct")
  end ;
  core##.notif##.origin := def (string "metal");
  show_op app core##.op

let show_batch ?network app ops =
  match Check_js.(carray (manager_operation notif_manager_info) ops) with
  | Error e ->
    let errors = List.map (fun e ->
        let code, content = error_content e in
        Mui.make_error code content) e in
    let errors_a = of_list errors in
    app##.core##.notif_error_ := def errors_a;
    app##.core##.forging := _false;
    error_callback (to_string app##.core##.notif##.id) errors_a;
    display_notif app
  | Ok ops ->
    app##.core##.notif##.batch := def ops;
    display_notif app;
    async_req
      ~error:(fun code content ->
          let data = Mui.make_error code content in
          app##.core##.notif_error_ := def @@ array [| data |];
          app##.core##.forging := _false;
          error_callback (to_string app##.core##.notif##.id) data)
      (forge_batch ?network app##.core ops)
      (fun () -> app##.core##.forging := _false)

let clear_op app kind =
  app##.path := string "home";
  let core = app##.core in
  match to_string kind with
  | "transaction" -> core##.op##.transaction := def (Dummy.trd ());
  | "origination" -> core##.op##.origination := def (Dummy.ord ())
  | "delegation" -> core##.op##.delegation := def (Dummy.dlg ())
  | "reveal" -> core##.op##.reveal := def (Dummy.rvl ())
  | "manage_account" -> core##.op##.manage_account_ := def (Dummy.man ())
  | "notarization" ->
    core##.op##.transaction := def (Dummy.trd ());
    core##.notarization_file_ := null;
    core##.notarization_comments_ := string ""
  | _ -> ()

let show_notif app notif =
  app##.core##.notif_error_ := undefined;
  app##.core##.notif := notif;
  app##.core##.forging := _true ;
  match to_string notif##.kind, Optdef.to_option notif##.op, Optdef.to_option notif##.batch with
  | "operation", Some op, _ ->
    let network = to_optdef Of_js.network notif##.network in
    show_op ?network app op
  | "approval", _, _ ->
    display_notif app
  | "batch", _, Some ops ->
    let network = to_optdef Of_js.network notif##.network in
    show_batch ?network app ops
  | _ -> ()

let clear_notif app =
  let modal : Vtypes.vue_modal t optdef = Vue_js.get_ref app "notif-modal" in
  begin match Optdef.to_option modal with | Some m -> m##hide | _ -> () end;
  app##.core##.notif_error_ := undefined;
  app##.core##.old_delegate_ := undefined;
  app##.core##.manage_account_admin_ := undefined;
  app##.core##.op := Dummy.op ();
  app##.core##.notif##.op := def (Dummy.op ())

let close_notif app =
  let modal : Vtypes.vue_modal t optdef = Vue_js.get_ref app "notif-modal" in
  begin match Optdef.to_option modal with | Some m -> m##hide | _ -> () end;
  Mui.set_timeout (fun () ->
      app##.core##.notif_error_ := undefined;
      app##.core##.old_delegate_ := undefined;
      app##.core##.manage_account_admin_ := undefined;
      app##.core##.op := Dummy.op ();
      app##.core##.notif##.op := def (Dummy.op ()))
    100.

let approve_url app =
  ok_notif app##.core##.notif undefined;
  clear_notif app

let register_new_kt1 app ~confirm op_bytes op_hash =
  let pkh = MCrypto.op_to_KT1 op_bytes in
  let msg = Unsafe.obj [||] in
  msg##.op_hash_ := string op_hash;
  msg##.contract := string pkh;
  let acc = Of_js.account app##.account in
  let network = Of_js.network app##.network in
  let callback _ = confirm msg in
  let error acc _ _ = Storage_writer.add_account ~callback acc in
  let callback_manager acc acc_info =
    let manager_kt = match acc_info.Dune.Types.node_ai_script with
      | Some (Some sc, _, _) -> Some (sc = Mrequest.Node.manager_kt)
      | _ -> Some false in
    Storage_writer.add_account ~callback { acc with manager_kt } in
  match acc.vault with
  | Local vault ->
    async_req
      (Vault.Lwt.decrypt ~pkh:acc.pkh ~vault >>=? fun sk ->
       Vault.Lwt.encrypt ~manager:acc.pkh ~pkh sk)
      (fun account_kt1 ->
         Mrequest.ENode.get_account_info ~network ~error:(error account_kt1) pkh
           (callback_manager account_kt1))
  | Ledger _ ->
    let account_kt1 = Mhelpers.dummy_account ~vault:acc.vault ~manager:acc.pkh pkh in
    Mrequest.ENode.get_account_info ~network ~error:(error account_kt1) pkh
      (callback_manager account_kt1)

let after_op app op_hash op_bytes =
  let core = app##.core in
  let op = Of_js.(to_optdef (manager_operation notif_manager_info) core##.notif##.op) in
  let confirm (msg :'a t optdef) =
    ok_notif core##.notif msg;
    (match Optdef.to_option core##.notif##.op with
     | None -> ()
     | Some op -> app##.path := string "home" ; refresh_history core op##.kind 0);
    clear_notif app;
    app##.core##.injecting := _false;
    (* update_pendings core *)
  in
  match op with
  | Some {mo_det = OriDetails _ord; _} ->
    register_new_kt1 core ~confirm op_bytes op_hash
  | _ -> confirm (def (string op_hash))

let inject_op app =
  let core = app##.core in
  core##.injecting := _true;
  let state = Of_js.state core##.account core##.network in
  let fg = core##.ml_forge_result_ in
  let network = to_optdef Of_js.network core##.notif##.network in
  let state = {state with fo_net = unopt state.fo_net network} in
  Mrequest.ENode.send_manager_operations ~state
    ~callback:(update_accounts core)
    ~error:(fun code content ->
        let data = Mui.make_error code content in
        core##.notif_error_ := def @@ array [| data |];
        error_callback (to_string core##.notif##.id) data;
        core##.injecting := _false)
    fg @@ fun (op_hash, op_bytes, _) ->
  after_op app op_hash op_bytes

let refuse_notif app =
  deny_notif app app##.core##.notif;
  clear_notif app

let add_to_batch app =
  let notif = app##.core##.notif in
  let data = object%js
    val notif = notif
    val time = app##.core##.postpone_time_
  end in
  let msg = Metal_message.mk_message ~data ~name:"callback_add_to_batch" ~src:"popup"
      (to_string notif##.id) in
  send_to_background msg;
  clear_notif app

let postpone_batch app =
  let data = object%js
    val time = app##.core##.postpone_time_
    val wid = app##.core##.notif##.wid
  end in
  let msg = Metal_message.mk_message ~data ~name:"callback_postpone" ~src:"popup"
      "postpone_batch" in
  send_to_background msg;
  clear_notif app

let load_config app =
  Storage_reader.get_config @@ fun settings ->
  app##.core##.batch_notif_ := bool settings.batch_notif

(* Main recursive function *)
let make_core (app:Vtypes.core_js t) acc accs net =
  update_networks app;
  app##.network := string @@ Mhelpers.network_to_string net;
  app##.account := acc;
  app##.accounts := accs;
  update_account app;
  update_bakers app

let rec make (app : Vtypes.data_js t) =
  Storage_reader.get_network @@ fun ho_net ->
  Storage_reader.get_account_full_js @@ fun selected accs ->
  match Optdef.to_option selected with
  | None -> Metal_start.make app
  | Some acc ->
    Vault.Cb.password @@ function
    | None -> Metal_unlock.make ~callback:make app
    | Some _ ->
      load_config app;
      let state =
        Of_js.state acc @@ string @@ Mhelpers.network_to_string ho_net in
      Mrequest.History.update_pending
        ~error:(fun _code _content ->
            make_core app##.core acc accs ho_net;
            app##.path := string "home")
        ~state
        (fun selected accs -> match Optdef.to_option selected with
           | None -> ()
           | Some acc ->
             make_core app##.core acc accs ho_net;
             app##.path := string "home")

(* computed properties *)

let change_network app =
  let net = Of_js.network app##.network in
  Storage_writer.update_network ~callback:(fun () ->
      update_account app;
      update_bakers app) net

let dunscan_path app =
  let network = Mhelpers.network_of_string @@ to_string app##.core##.network in
  def @@ string @@ Mhelpers.dunscan_path network

let revealed app =
  match get_revealed app##.core with
  | Some op_hash ->
    def @@ object%js
      val txt = string "Revealed"
      val hash = def (string op_hash)
    end
  | _ ->
    def @@ object%js
      val txt = string "Unrevealed"
      val hash = undefined
    end

let dune_script _app s contract =
  let contract = to_bool contract in
  match Dune.parse_script ~contract (to_string s) with
  | Error _ -> s
  | Ok sc -> string @@ Dune.print_script ~contract sc

let pp_date _app s =
  let date_cs = Unsafe.global##._Date in
  let d = new%js date_cs s in
  d##toLocaleString (string "en-GB")

let pp_ago _app s =
  Mui.ago_str @@ Mui.float_of_iso (to_string s)

let op_row_class _app item typ =
  if typ <> string "row" then undefined
  else match Optdef.to_option item with
    | None -> undefined
    | Some op ->
      let op = Of_js.block_operation op in
      if op.bo_block = None then def @@ string "bg-warning"
      else
        match List.nth_opt op.bo_op 0 with
        | None -> undefined
        | Some op ->
          if op.mo_info.mi_failed then def @@ string "bg-danger"
          else undefined

let set_manage_account_json app =
  Mui.wait @@ fun () ->
  match Opt.to_option app##.manage_account_json_ with
  | None -> ()
  | Some f -> match Opt.to_option @@ File.CoerceTo.blob f with
    | None -> ()
    | Some f -> Mui.read_file f (fun s ->
        match Optdef.to_option app##.op##.manage_account_ with
        | None -> ()
        | Some details -> details##.json := def (string s))

let copy app s =
  let clipboard = Unsafe.variable "navigator.clipboard" in
  Unsafe.(coerce clipboard)##writeText s |> ignore;
  app##.core##.copied := _true;
  Mui.wait ~t:5000. (fun () -> app##.core##.copied := _false)

let lock app =
  Vault.Cb.lock ~callback:(fun _ ->
      let data = object%js val msg = string "metal locked" end in
      Notif_background.send ~data () ;
      make app) ()

let init () =
  let core f app = f app##.core in
  V.add_method0 "change_network" (core change_network);
  V.add_method1 "deny_notif" deny_notif;
  V.add_method1 "change_account" change_account;
  V.add_method0 "get_import_accounts" (core get_import_accounts);
  V.add_method0 "import_accounts" (core import_accounts);
  V.add_method0 "clear_import" (core clear_import);
  V.add_method1 "clear_op" clear_op;
  V.add_method2 "show_op" show_op_kind;
  V.add_method1 "show_notif" show_notif;
  V.add_method0 "approve_url" approve_url;
  V.add_method0 "inject_op" inject_op;
  V.add_method0 "refuse_notif" refuse_notif;
  V.add_method2 "dune_script" dune_script;
  V.add_method2 "refresh_history" (core refresh_history);
  V.add_computed "dunscan_path" dunscan_path;
  V.add_method0 "set_manage_account_json" (core set_manage_account_json);
  V.add_computed "revealed" revealed;
  V.add_computed "notarization_contract" notarization_contract;
  V.add_method1 "pp_date" pp_date;
  V.add_method1 "pp_ago" pp_ago;
  V.add_method2 "op_row_class" op_row_class;
  V.add_method1 "copy" copy;
  V.add_method0 "lock" lock;
  V.add_method0 "close_notif" close_notif;
  V.add_method0 "add_to_batch" add_to_batch;
  V.add_method0 "postpone_batch" postpone_batch;
  object%js
    val mutable network = string "Mainnet"
    val mutable networks =
      of_listf (fun n -> string @@ Mhelpers.network_to_string n) Mhelpers.network_options
    val mutable account = Dummy.account_full ()
    val mutable accounts = array [||]
    val mutable balance = undefined
    val mutable account_info_ = undefined
    val mutable notifications = array [||]
    val mutable import_accounts_ = array [||]
    val mutable selected_import_accounts_ = array [||]
    val mutable importation = _false
    val mutable notif = Dummy.notif ()
    val mutable op = Dummy.op ()
    val mutable op_bytes_ = string ""
    val mutable op_result_ = Dummy.op_result ()
    val mutable ml_forge_result_ = {fg_bytes = Bigstring.empty; fg_protocol = ""; fg_branch = ""; fg_ops = []}
    val mutable injecting = _false
    val mutable forging = _false
    val mutable notif_error_ = undefined
    val mutable old_delegate_ = undefined
    val mutable manage_account_admin_ = undefined
    val mutable notarization_file_ = null
    val mutable notarization_comments_ = string ""
    val mutable manage_account_json_ = null
    val mutable transaction_history_ = Dummy.history ()
    val mutable origination_history_ = Dummy.history ()
    val mutable delegation_history_ = Dummy.history ()
    val mutable notarization_history_ = Dummy.history ()
    val mutable manage_account_history_ = Dummy.history ()
    val mutable operation_tab_ = 0
    val mutable notarization_info_ = Jstable.create ()
    val mutable copied = _false
    val mutable delegate = undefined
    val mutable bakers = array [||]
    val mutable notifs_list_visible_ = _false
    val mutable tr_expanded_ = _false
    val mutable del_expanded_ = _false
    val mutable postpone_time_ = 0
    val mutable batch_notif_ = _false
  end
