(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Chrome
open Ezjs_min
open Storage_utils
open Metal_message_types
open Metal_types

let close_notif _app =
  Windows.getCurrent @@ fun win ->
  match Optdef.to_option win##.id with
  | Some id -> Windows.remove id
  | _ -> ()

let make_notif app wid rid typ =
  let msg : _ message t = match typ with
    | "approve" -> Metal_message.mk_metadata_req "popup" rid
    | "send" | "delegate" | "originate" -> Metal_message.mk_op_req "popup" rid
    | "batch" -> Metal_message.mk_batch_req "popup" rid
    | "commit_batch" | "loop_batch" ->
      Metal_message.mk_message ~name:"get_batch_store" ~src:"popup" rid
    | _ -> log_str "cannot send message"; assert false in
  let info = Utils.Runtime.mk_connection_info "popup" in
  let port = Runtime.connect ~info () in
  port##postMessage msg ;
  Utils.Browser.addListener1 port##.onMessage @@ fun (ans : _ answer t) ->
  if ans##.src = string "background" && msg##.req##.id = ans##.res##.id then
    let acc = Of_js.account app##.core##.account in
    let tsp = to_string (new%js date_now)##toLocaleString in
    let origin = match Optdef.to_option port##.sender with
      | None -> "unknown"
      | Some sender -> match Optdef.to_option sender##.url with
        | None -> "unknown"
        | Some origin -> to_string origin in
    let notif_js = Dummy.notif ~tsp ~origin ~id:rid ~wid () in
    notif_js##.op := undefined;
    (match typ with
     | "send" | "delegate" | "originate"->
       let op : op t = Unsafe.coerce ans##.res##.result in
       notif_js##.op := def op##.op;
       notif_js##.network := convdef (fun x -> To_js.network @@ Of_js.network x) op##.network;
       notif_js##.type_ := op##.type_;
       notif_js##.kind := string "operation"
     | "approve" ->
       let metadata : site_metadata t = Unsafe.coerce ans##.res##.result in
       notif_js##.icon := metadata##.icon;
       notif_js##.name := def metadata##.name;
       notif_js##.url := def metadata##.url;
       notif_js##.kind := string "approval"
     | "batch" | "commit_batch" | "loop_batch" ->
       let batch : batch t = Unsafe.coerce ans##.res##.result in
       notif_js##.batch := def batch##.ops;
       notif_js##.network := convdef (fun x -> To_js.network @@ Of_js.network x) batch##.network;
       notif_js##.type_ :=
         if typ = "commit_batch" then def (string "commit")
         else if typ = "loop_batch" then def (string "loop")
         else batch##.type_;
       notif_js##.kind := string "batch"
     | _ -> ());
    let notif = Storage_utils.Of_js.notif notif_js in
    if typ <> "commit_batch" && typ <> "loop_batch" then
      Storage_reader.get_notifs (fun ns ->
          let notifs = (
            MMisc.unopt {notif_acc=acc.pkh; notifs=[]}
              (List.find_opt (fun {notif_acc ; _} -> notif_acc = acc.pkh) ns)).notifs in
          let n_notif = List.length notifs in
          if (List.exists (fun n -> Mhelpers.notif_wid_of_not n = wid) notifs) then
            MCore.show_notif app (To_js.notif notif)
          else
            Storage_writer.add_notif
              ~callback:(fun _ ->
                  Browser_action.set_badge ~text:(string_of_int @@ n_notif + 1) () ;
                  MCore.show_notif app notif_js)
              acc.pkh notif)
    else
      match to_optdef to_list notif_js##.batch with
      | Some (_h :: _t) -> MCore.show_notif app notif_js
      | _ -> ()

let fill_error app s =
  let error = Mui.make_error 0 (Some s) in
  app##.core##.notif_error_ := def @@ of_list [ error ]

let dispatch (app:Vtypes.data_js t) wid args =
  match List.assoc_opt "type" args with
  | None -> fill_error app "No type argument"
  | Some typ ->
    Storage_reader.get_network @@ fun ho_net ->
    Storage_reader.get_account_full_js @@ fun selected _accs ->
    match Optdef.to_option selected with
    | None when typ = "enable" -> app##.path := string "start"
    | Some acc ->
      MCore.load_config app;
      Vault.Cb.password (function
          | None when typ = "unlock" ->
            Metal_unlock.make ~callback:(fun _ -> close_notif app) app
          | Some _ -> (match typ with
              | "approve" | "send" | "originate" | "delegate" | "batch"
              | "commit_batch" | "loop_batch" ->
                (match List.assoc_opt "req_id" args, Optdef.to_option wid with
                 | Some rid, Some wid ->
                   Storage_reader.get_config @@ fun settings ->
                   app##.core##.batch_notif_ := bool settings.batch_notif;
                   app##.core##.network := To_js.network ho_net;
                   app##.core##.account := acc;
                   make_notif app wid rid typ
                 | _ -> fill_error app "Cannot find request ID or window ID")
              | _ -> fill_error app ("notif type \"" ^ typ ^ "\" not understood"))
          | _ -> fill_error app "Cannot handle this notif when locked")
    | _ -> fill_error app "Cannot handle this notif whithout account"

let set_body_dim () =
  let notif_height = 600 in
  let notif_width = 357 in
  Tabs.getZoom @@ fun z ->
  Dom_html.document##.body##.style##.width :=
    string (string_of_float (float_of_int notif_width /. z) ^ "px");
  Dom_html.document##.body##.style##.height :=
    string (string_of_float (float_of_int notif_height /. z) ^ "px")

let () =
  set_body_dim ();
  let unlock = Metal_unlock.init () in
  let core = MCore.init () in
  V.add_method0 "close_notif" close_notif;
  let theme = Theme.light_theme in
  let app = V.init ~path:"loading" ~unlock ~core ~theme
      ~render:Render.notification_render ~static_renders:Render.notification_static_renders ()  in
  Windows.getCurrent @@ fun win ->
  dispatch app win##.id Url.Current.arguments
