open Js_of_ocaml
open Js

let dark_theme : Vtypes.theme_js t = object%js
  val bg_variant_ = some (string "dark")
  val tx_variant_ = some (string "light")
  val bd_variant_ = some (string "light")
  val variant = some (string "light bg-dark ")
  val class_ = some (string "bg-dark text-light")
  val border_class_ = some (string "border-light")
  val type_ = some (string "dark")
  val outline = some (string "outline-light")
  val dark = _true
  val icon = string "sun"
  val icon_variant_ = some (string "warning")
end

let light_theme : Vtypes.theme_js t = object%js
  val bg_variant_ = some (string "light")
  val tx_variant_ = some (string "dark")
  val bd_variant_ = null
  val variant = null
  val class_ = some (string "bg-light text-dark")
  val border_class_ = null
  val type_ = null
  val outline = some (string "outline-secondary")
  val dark = _false
  val icon = string "moon"
  val icon_variant_ = null
end

let empty : Vtypes.theme_js t = object%js
  val bg_variant_ = null
  val tx_variant_ = null
  val bd_variant_ = null
  val variant = null
  val class_ = null
  val border_class_ = null
  val type_ = null
  val outline = null
  val dark = _false
  val icon = string "moon"
  val icon_variant_ = null
end

let init f =
  Storage_reader.get_config (fun {Metal_types.theme; _} ->
      let th =
        if theme = "dark" then dark_theme
        else if theme = "light" then light_theme
        else empty in
      f th
    )

let change_theme app theme =
  ignore @@ Unsafe.meth_call Dom_html.window "change_theme" [| Unsafe.inject (string theme) |];
  if theme = "dark" then app##.theme := dark_theme
  else if theme = "light" then app##.theme := light_theme
  else app##.theme := empty
