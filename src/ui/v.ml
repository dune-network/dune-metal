open Vtypes
open Ezjs_min

include Vue_js.Make(struct
    type data = data_js
    type all = data
    let id = "app" end)

let init
    ?(enabled=_true) ?(start=Dummy.start ()) ?(pres=Dummy.presentation ())
    ?(reset=Dummy.reset ()) ?(settings=Dummy.settings ())
    ~path ~unlock ~core ~theme ~render ~static_renders () =
  let data = object%js
    val mutable path = string path
    val mutable enabled = enabled
    val mutable start = start
    val mutable pres = pres
    val mutable unlock = unlock
    val mutable reset = reset
    val mutable settings = settings
    val mutable core = core
    val mutable theme = theme
  end in
  init ~show:true ~export:Menv.dev ~data ~render ~static_renders ()

let () = Vc.load ()
