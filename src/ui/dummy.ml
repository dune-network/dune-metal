open Ezjs_min
open Storage_utils

let vault () : vault_js t = object%js
  val kind = string ""
  val iv = undefined
  val esk = undefined
  val path = undefined
  val pwd = undefined
end

let account () : account_js t = object%js
  val index = -1
  val pkh = string ""
  val manager = string ""
  val vault = vault ()
  val name = string ""
  val revealed = array [||]
  val managerKT = undefined
  val admin = array [||]
end

let account_full () : account_full_js t = object%js
  val index = -1
  val pkh = string ""
  val manager = string ""
  val vault = vault ()
  val name = string ""
  val revealed = array [||]
  val managerKT = undefined
  val admin = array [||]
  val pending = array [||]
  val history = array [||]
end

let manager_info () : notif_manager_info_js t = object%js
  val mutable fee = undefined
  val mutable gasLimit = undefined
  val mutable storageLimit = undefined
end

let parameters () : parameters_js t = object%js
  val mutable entrypoint = undefined
  val mutable value = undefined
end

let trd () : transaction_details_js t = object%js
  val mutable destination = string ""
  val mutable amount = string ""
  val mutable currency = def (string "dun")
  val mutable parameters = def (parameters ())
  val mutable collectCall = _false
end

let script () : script_js t = object%js
  val mutable code = undefined
  val mutable storage = string ""
  val mutable codeHash = undefined
end

let ord () : origination_details_js t = object%js
  val mutable balance = string ""
  val mutable currency = def (string "dun")
  val mutable kt1 = undefined
  val mutable delegate = undefined
  val mutable script = def (script ())
end

let dlg () : delegation_details_js t = object%js
  val mutable delegate = undefined
end

let rvl () : reveal_details_js t = object%js
  val mutable pubkey = string ""
end

let man () : manage_account_details_js t = object%js
  val mutable target = undefined
  val mutable maxrolls = undefined
  val mutable set_maxrolls_ = _false
  val mutable admin = undefined
  val mutable set_admin_ = _false
  val mutable whiteList = undefined
  val mutable delegation = undefined
  val mutable recovery = undefined
  val mutable set_recovery_ = _false
  val mutable actions = array [||]
  val mutable json = undefined
end

let op () : notif_manager_info_js t manager_operation_js t = object%js
  val mutable info = manager_info ()
  val mutable kind = string ""
  val mutable transaction = def (trd ())
  val mutable origination = def (ord ())
  val mutable delegation = def (dlg ())
  val mutable reveal = def (rvl ())
  val mutable manage_account_ = def (man ())
end

let notif ?network ?typ ?(kind="") ?(id="") ?(wid=0) ?(tsp="") ?origin () : notif_js t = object%js
  val mutable kind = string kind
  val mutable id = string id
  val mutable wid = wid
  val mutable tsp = string tsp
  val mutable origin = optdef string origin
  val mutable op = def (op ())
  val mutable batch = undefined
  val mutable network = optdef To_js.network network
  val mutable type_ = optdef string typ
  val mutable icon = undefined
  val mutable name = undefined
  val mutable url = undefined
end

let history () : Vtypes.history_js t = object%js
  val mutable ops = array [||]
  val mutable page = 1
  val mutable total = 10
end

let op_result () : Vtypes.op_result t = object%js
  val amount = string ""
  val fee = string ""
  val burn = string ""
  val gas_limit_ = string ""
  val storage_limit_ = string ""
end

let start () : Vtypes.start_js t = object%js
  val mutable section = string ""
  val mutable account = undefined
  val mutable vault = object%js
    val pkh = string ""
    val sk = undefined
    val path = undefined
  end
  val mutable network = string "Mainnet"
  val mutable networks = array [||]
  val mutable file_ico_ = null
  val mutable mnemonic_ico_ = string ""
  val mutable email_ico_ = string ""
  val mutable pwd_ico_ = string ""
  val mutable pkh_ico_ = string ""
  val mutable activation_ico_ = string ""
  val mutable json_ico_state_ = null
  val mutable mnemonic_seed_ = string ""
  val mutable passphrase_seed_ = string ""
  val mutable mnemonic_seed_state_ = null
  val mutable secret_key_ = string ""
  val mutable secret_key_pwd_ = string ""
  val mutable secret_key_decrypting_ = _false
  val mutable secret_key_state_ = null
  val mutable hardware = string "ledger"
  val mutable derivation_path_ = string "44'/1729'/0'/0'"
  val mutable mnemonic_new_ = string ""
  val mutable passphrase_new_ = string ""
  val mutable encrypt_pwd1_ = string ""
  val mutable encrypt_pwd2_ = string ""
  val mutable encrypt_pwd_ = string ""
  val mutable pwd1_state_ = null
  val mutable pwd2_state_ = null
  val mutable pwd_state_ = null
end

let presentation () : Vtypes.pres_js t =
  object%js
    val mutable step = 1
  end

let reset () : Vtypes.reset_js t =
  object%js
    val mutable confirmation = _false
    val mutable textarea_rows_ = 10
    val mutable accounts = string ""
  end

let settings () : Vtypes.settings_js t =
  object%js
    val mutable use_dunscan_ = _true
    val mutable theme = string "default"
    val mutable batch_notif_ = _false
    val mutable notif_timeout_ = number_of_float 20000.
    val mutable accounts = array [||]
    val mutable accounts_secret_ = array [||]
    val mutable account_secret_ = string ""
    val mutable pwd_secret_ = string ""
    val mutable secret_key_ = string ""
    val mutable account_secret_state_ = null
    val mutable pwd_secret_state_ = null
    val mutable old_pwd_ = string ""
    val mutable new_pwd_ = string ""
    val mutable confirm_pwd_ = string ""
    val mutable old_pwd_state_ = null
    val mutable new_pwd_state_ = null
    val mutable confirm_pwd_state_ = null
    val mutable networks = array [||]
    val mutable network_node_ = string ""
    val mutable network_scan_ = string ""
    val mutable network_name_ = string ""
    val mutable network_node_state_ = null
    val mutable network_scan_state_ = null
    val mutable network_name_state_ = null
    val mutable remove = array [||]
    val mutable export_password_ = string ""
    val mutable export_state_ = null
    val mutable download_href_ = undefined
    val mutable import_state_ = null
    val mutable import_file_ = null
    val mutable import_string_ = undefined
    val mutable local_cleared_ = _false
  end
