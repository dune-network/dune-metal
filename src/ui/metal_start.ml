(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ezjs_min
open MCrypto
open Metal_types
open Storage_utils

let section app s =
  begin match to_string s with
    | "" -> Storage_reader.get_account_js (fun account _accs -> app##.account := account)
    | "import" ->
      let networks =
        List.map (fun n -> string @@ Mhelpers.network_to_string n) Mhelpers.network_options in
      Storage_reader.get_custom_networks (fun l ->
          let cnetworks = List.map (fun (name, _, _) -> string name) l in
          app##.networks := of_list (networks @ cnetworks))
    | "new" ->
      let x = Hacl.Rand.gen 20 in
      let words = Bip39.to_words @@ Bip39.of_entropy x in
      app##.mnemonic_new_ := string (String.concat " " words)
    | _ -> ()
  end;
  app##.section := s

let cancel app = section app (string "")

let store_vault app account =
  Storage_writer.add_account ~select:true ~callback:(fun _ -> section app (string "end")) account

let encrypt app =
  let pkh = to_string app##.vault##.pkh in
  let aux () =
    match to_optdef to_string app##.vault##.sk, to_optdef to_string app##.vault##.path with
    | Some sk, _ ->
      let sk = Sk.b58dec sk in
      Vault.Cb.encrypt
        ~error:(fun code content ->
            log "error in make local vault: %d %s" code (MMisc.unopt "" content))
        ~pkh sk (store_vault app)
    | _, Some path ->
      Vault.Cb.hash_password
        ~error:(fun code content ->
            log "error in make ledger vault: %d %s" code (MMisc.unopt "" content))
        ~pkh path (store_vault app)
    | _ -> () in
  match to_optdef Of_js.account app##.account with
  | None ->
    let pwd1 = to_string app##.encrypt_pwd1_ in
    let pwd2 = to_string app##.encrypt_pwd2_ in
    if pwd1 = pwd2 then
      if String.length pwd1 >= 8 then
        Vault.Cb.unlock_first ~callback:(fun _ -> aux ()) pwd1
      else app##.pwd1_state_ := some _false
    else app##.pwd2_state_ := some _false
  | Some account ->
    let pwd = to_string app##.encrypt_pwd_ in
    Vault.Cb.password (function
        | None ->
          Vault.Cb.unlock
            ~error:(fun _ _ -> app##.pwd_state_ := some _false)
            ~callback:(fun _ -> aux ())
            account pwd
        | Some password ->
          if password <> pwd then app##.pwd_state_ := some _false
          else aux ())

let activate ?activate_info pkh = match activate_info with
  | Some (code, network) when code <> "" ->
    let network = Mhelpers.network_of_string network in
    let state = Mhelpers.dummy_fo_state ~network pkh in
    Mrequest.History.get_activated ~state
      ~error:(fun code msg ->
          log "cannot get activation from dunscan: %d %s" code (MMisc.unopt "" msg))
      (function
        | false ->
          Mrequest.ENode.send_activation ~network
            ~error:(fun code msg ->
                log "activation failed %d %s" code (MMisc.unopt "" msg))
             pkh code
             (fun (h, _ops) -> log "activation %s injected" (Operation_hash.b58enc h))
        | _ -> log "account %s already activated" pkh
      )
  | _ -> ()

let vault_from_mnemonic mnemonic passphrase =
  let indices = Bip39.of_words mnemonic in
  match indices with
  | None -> assert false
  | Some indices ->
    let passphrase = Bigstring.of_string passphrase in
    let sk = Bigstring.sub (Bip39.to_seed ~passphrase indices) 0 32 in
    Sk.b58enc sk, Vault.pkh_from_sk sk

let extract_secret_key app sk password f =
  let n = String.length sk in
  if n < 12 then f None
  else if String.sub sk 0 11 = "unencrypted" then f (Some (String.sub sk 12 (n - 12)))
  else if String.sub sk 0 4 = "edsk" then f (Some sk)
  else (
    let edesk =
      if String.sub sk 0 9 = "encrypted" then String.sub sk 10 (n - 10)
      else sk in
    try
      let edesk_b = Base58.decode Prefix.ed25519_encrypted_seed edesk in
      app##.secret_key_decrypting_ := _true;
      Mui.wait (fun () ->
          match Box.decrypt ~password edesk_b with
          | None ->
            app##.secret_key_decrypting_ := _false;
            f None
          | Some b ->
            app##.secret_key_decrypting_ := _false;
            f (Some (Base58.encode Prefix.ed25519_seed b)))
    with _ -> f None)

let make_local_vault ?error ?activate_info ?secret_key ?mnemonic ?(passphrase="") app =
  try
    let sk_b58, pkh_b58 = match secret_key, mnemonic with
      | Some sk_b58, _ ->
        let sk = Sk.b58dec sk_b58 in
        Sk.b58enc sk, Vault.pkh_from_sk sk
      | _, Some mnemonic ->
        vault_from_mnemonic mnemonic passphrase
      | _ -> assert false in
    activate ?activate_info pkh_b58;
    app##.vault := object%js
      val pkh = string pkh_b58
      val sk = def (string sk_b58)
      val path = undefined
    end;
    section app (string "encrypt")
  with _ -> match error with None -> () | Some error -> error ()

let make_ledger_vault ~path ~pkh app =
  app##.vault := object%js
    val pkh = string pkh
    val sk = undefined
    val path = def (string path)
  end;
  section app (string "encrypt")

let load_json_ico app =
  Mui.wait @@ fun () ->
  match Opt.to_option app##.file_ico_ with
  | None -> app##.json_ico_state_ := some _false
  | Some f -> match Opt.to_option @@ File.CoerceTo.blob f with
    | None -> app##.json_ico_state_ := some _false
    | Some f ->
      Mui.read_file f (fun s ->
          let faucet = EzEncoding.destruct Metal_encoding.faucet s in
          app##.mnemonic_ico_ := string (String.concat " " faucet.fc_mnemonic);
          app##.email_ico_ := string faucet.fc_email;
          app##.pwd_ico_ := string faucet.fc_password;
          app##.pkh_ico_ := string faucet.fc_pkh;
          app##.activation_ico_ := string faucet.fc_code)

let create app s =
  match to_string s with
  | "ico" ->
    let mnemonic = String.split_on_char ' ' (to_string app##.mnemonic_ico_) in
    let passphrase = (to_string app##.email_ico_) ^ (to_string app##.pwd_ico_) in
    let activate_info = to_string app##.activation_ico_, to_string app##.network in
    make_local_vault ~activate_info ~mnemonic ~passphrase app
  | "seed" ->
    let mnemonic = String.split_on_char ' ' (to_string app##.mnemonic_seed_) in
    let passphrase = to_string app##.passphrase_seed_ in
    make_local_vault ~error:(fun () -> app##.mnemonic_seed_state_ := some _false)
      ~mnemonic ~passphrase app
  | "sk" ->
    let secret_key = to_string app##.secret_key_ in
    let password = to_string app##.secret_key_pwd_ in
    app##.secret_key_decrypting_ := _true;
    extract_secret_key app secret_key password (fun sk ->
        app##.secret_key_pwd_ := string "";
        match sk with
        | None ->
          app##.secret_key_state_ := some _false
        | Some secret_key ->
          make_local_vault ~error:(fun () -> app##.secret_key_state_ := some _false)
            ~secret_key app)
  | "new" ->
    let mnemonic = String.split_on_char ' ' (to_string app##.mnemonic_new_) in
    let passphrase = to_string app##.passphrase_new_ in
    make_local_vault ~mnemonic ~passphrase app
  | "ledger" ->
    let path = to_string app##.derivation_path_ in
    MLwt.async_req
      ~error:(fun _code _content -> ())
      (Ledger_js.getAddress_bg path)
      (fun (_pk, pkh) -> make_ledger_vault ~pkh ~path app)
  | _ -> ()

let init () =
  let start f app = f app##.start in
  V.add_method1 "section" (start section);
  V.add_method1 "create" (start create);
  V.add_method0 "load_json_ico" (start load_json_ico);
  V.add_method0 "encrypt" (start encrypt);
  Dummy.start ()

let make app =
  section app##.start (string "");
  app##.path := string "start";
