open Ezjs_min

let route app path =
  js_log path;
  app##.path := string "loading";
  match to_string path with
  | "start" -> Metal_start.make app
  | "unlock" -> Metal_unlock.make ~callback:MCore.make app
  | "settings"-> Metal_settings.make app
  | "reset" -> Metal_reset.make app
  | "home" -> MCore.make app
  | "presentation" -> app##.path := string "presentation"
  | _ -> ()

let remove_arguments () =
  let path = some @@ string "/index.html" in
  Dom_html.window##.history##pushState path (string "") path

let () =
  V.add_method1 "route" route;
  let start = Metal_start.init () in
  let pres = Dummy.presentation () in
  let unlock = Metal_unlock.init () in
  let reset = Metal_reset.init () in
  let settings = Metal_settings.init () in
  let core = MCore.init () in
  let path = MMisc.unopt "home" (List.assoc_opt "path" Url.Current.arguments) in
  remove_arguments ();
  Storage_reader.is_enabled @@ fun enabled ->
  let enabled = bool enabled in
  Theme.init @@ fun theme ->
  let app = V.init ~path ~enabled ~start ~pres ~unlock ~reset ~settings ~core
      ~theme ~render:Render.home_render ~static_renders:Render.home_static_renders () in
  route app (string path)
