open Ezjs_min
open Ezjs_dom

let ledger_iframe_id = "ledger-iframe"

class type ledgerParams = object
  method path : js_string t prop
  method data : js_string t opt prop
  method success : bool t opt prop
end

class type ledgerMessage = object
  method action : js_string t prop
  method target : js_string t opt prop
  method params : ledgerParams t prop
end

let make_iframe () =
  let iframe = El.iframe
      ~attr:["src", "https://dune.network/ledger-bridge/index.html?version=0"; "id", ledger_iframe_id] [] in
  appendChild Dom_html.document##.head iframe;
 |> ignore

let send_message (msg : ledgerMessage t) =
  match Opt.to_option @@ Dom_html.CoerceTo.iframe (by_id ledger_iframe_id) with
  | None -> ()
  | Some ledger_iframe ->
    msg##.target := some (string "DUNE-METAL-BG-LEDGER-IFRAME");
    (Unsafe.coerce ledger_iframe##.contentWindow)##postMessage msg (string "*")

let iframe_listener expected_action cb =
  Js_of_ocaml.Dom_events.listen
    Dom_html.window
    (Dom_html.Event.make "message")
    (fun _target ev ->
       let action = to_string ev##.data##.action in
       let target = to_string ev##.data##.target in
       if (action = expected_action) && target = "DUNE-METAL-BG" then
         cb ev##.data##.params;
       false)
