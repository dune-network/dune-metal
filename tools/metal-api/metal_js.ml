open Js_of_ocaml
open Js

class type network = object
  method ok : bool t readonly_prop
  method name : js_string t readonly_prop
  method url : js_string t readonly_prop
end

class type op_result = object
  method ok : bool t optdef readonly_prop
  method msg : _ optdef readonly_prop
end

class type send_result = object
  inherit op_result
  inherit js_string
end

class type originate_msg = object
  method op_hash : js_string t readonly_prop
  method contract : js_string t readonly_prop
end

class type originate_result = object
  inherit op_result
  inherit originate_msg
end

class type info_param = object
  method fee : js_string t optdef readonly_prop (* in mudun *)
  method gas_limit_ : js_string t optdef readonly_prop
  method storage_limit_ : js_string t optdef readonly_prop
end

class type ['cb] meth_config = object
  method network : js_string t optdef readonly_prop
  method cb : ('cb -> unit) callback optdef readonly_prop
end

class type ['cb] op_config = object
  inherit ['cb] meth_config
  method type_ : js_string t optdef readonly_prop
end

class type send_param = object
  method dst : js_string t readonly_prop
  method amount : js_string t readonly_prop (* in udun float *)
  method collect_call_ : bool t optdef readonly_prop
  method parameter : js_string t optdef readonly_prop (* in json/michelson/love *)
  method entrypoint : js_string t optdef readonly_prop (* in json *)
  inherit [send_result t] op_config
  inherit info_param
end

class type originate_param = object
  method balance : js_string t readonly_prop (* in mudun *)
  method sc_code_hash_ : js_string t optdef readonly_prop (* in json *)
  method sc_storage_ : js_string t optdef readonly_prop (* in json *)
  method sc_code_ : js_string t optdef readonly_prop (* in json *)
  inherit [originate_result t] op_config
  inherit info_param
end

class type delegate_param = object
  method delegate : js_string t optdef readonly_prop
  inherit [send_result t] op_config
  inherit info_param
end

class type batch_operation = object
  method kind : js_string t prop
  method operation_transaction : send_param t optdef prop
  method operation_origination : originate_param t optdef prop
  method operation_delegation : delegate_param t optdef prop
end

class type batch_param = object
  method operations : batch_operation t js_array t readonly_prop
  inherit [send_result t] op_config
end

class type ['output] rpc_param = object
  method rpc : js_string t readonly_prop
  inherit ['output] meth_config
end

class type metal = object
  method isEnabled : (bool t -> unit) -> unit meth
  method isUnlocked : (bool t -> unit) -> unit meth
  method isApproved : (bool t -> unit) -> unit meth
  method getAccount : (js_string t -> unit) -> unit meth
  method getNetwork : (network t -> unit) -> unit meth

  method onStateChanged : ('b t -> unit) -> unit meth

  method send : send_param t -> unit meth
  method originate : originate_param t -> unit meth
  method delegate : delegate_param t -> unit meth
  method batch : batch_param t -> unit meth

  method rpc : 'output rpc_param t -> unit meth
  method head : 'a t meth_config t optdef  -> unit meth

  method commit : send_result t meth_config t optdef -> unit meth
end

let ready ?(not_installed=fun () -> ()) ?(timeout=500.) f =
  Optdef.case Unsafe.global##.metal
    (fun () ->
       let cb () =
         Optdef.case Unsafe.global##.metal not_installed f in
       ignore @@ Dom_html.window##setTimeout (wrap_callback cb) timeout)
    f
